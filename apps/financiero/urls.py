from django.contrib import admin
from django.contrib.auth.decorators import login_required
from django.urls import path, include
from apps.financiero import views


urlpatterns = [
    path('', login_required(views.listFinancieroGeneral_view), name='listFinancieroGeneral'),
    #path('listFinanciero/comite/<int:id>/', views.listFinancieroComite_view, name='listFinancieroComite'),
    path('listFinanciero/municipio/comite/<int:id>/', views.listFinancieroMunicipio_view, name='listFinancieroMunicipio'),
    path('formFinanciero/', login_required(views.formFinanciero_view), name='formFinanciero'),
    path('updaFinanciero/<int:id>/', login_required(views.updaFinanciero_view), name='updaFinanciero'),
    #path('regiFinanciero/<int:id>/', views.regiFinanciero_view, name='regiFinanciero'),
    path('borrFinanciero/', login_required(views.BorrFinanciero_view.as_view()), name='borrFinanciero'),
    path('buscSaldoFinal/', login_required(views.buscSaldoFinal_view), name='buscSaldoFinal'),
    path('buscYaRegistrado/', login_required(views.buscYaRegistrado_view), name='buscYaRegistrado'),
    path('repoGeneral/', login_required(views.repoGeneral_view), name='repoGeneral'),
    path('repoGeneralAnterior/', login_required(views.repoGeneralAnterior_view), name='repoGeneralAnterior'),
]
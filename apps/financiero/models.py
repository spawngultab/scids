from django.db import models
from apps.comite.models import Comite


class Corte(models.Model):
    comite                        = models.ForeignKey(Comite, on_delete=models.CASCADE, blank=True, null=True)
    fecha_corte                   = models.DateField(blank=True, null=True)
    periodo_evaluado_inicio       = models.DateField(blank=True, null=True)
    periodo_evaluado_fin          = models.DateField(blank=True, null=True)
    folios_utilizados             = models.TextField(max_length=900, default='', blank=True, null=True)
    saldo_caja                    = models.DecimalField(max_digits=7, decimal_places=2, blank=True, null=True)
    ingresos_porContratos         = models.DecimalField(max_digits=7, decimal_places=2, blank=True, null=True)
    ingresos_porCuotasMensuales   = models.DecimalField(max_digits=7, decimal_places=2, blank=True, null=True)
    ingresos_porAportaciones      = models.DecimalField(max_digits=7, decimal_places=2, blank=True, null=True)
    ingresos_porOtros             = models.DecimalField(max_digits=7, decimal_places=2, blank=True, null=True)
    total_ingresos                = models.DecimalField(max_digits=7, decimal_places=2, blank=True, null=True)
    saldo_caja_mas_ingresos       = models.DecimalField(max_digits=7, decimal_places=2, blank=True, null=True)
    egresos_compensacionBombero   = models.DecimalField(max_digits=7, decimal_places=2, blank=True, null=True)
    egresos_refaccionesMateriales = models.DecimalField(max_digits=7, decimal_places=2, blank=True, null=True)
    egresos_manoObra              = models.DecimalField(max_digits=7, decimal_places=2, blank=True, null=True)
    egresos_energiaElectrica      = models.DecimalField(max_digits=7, decimal_places=2, blank=True, null=True)
    egresos_pasajesViaticos       = models.DecimalField(max_digits=7, decimal_places=2, blank=True, null=True)
    egresos_papeleria             = models.DecimalField(max_digits=7, decimal_places=2, blank=True, null=True)
    egresos_telefono              = models.DecimalField(max_digits=7, decimal_places=2, blank=True, null=True)
    egresos_gratificaciones       = models.DecimalField(max_digits=7, decimal_places=2, blank=True, null=True)
    egresos_compraEquipo          = models.DecimalField(max_digits=7, decimal_places=2, blank=True, null=True)
    egresos_otros                 = models.DecimalField(max_digits=7, decimal_places=2, blank=True, null=True)
    egresos_contratos             = models.DecimalField(max_digits=7, decimal_places=2, blank=True, null=True)
    egresos_combustibles          = models.DecimalField(max_digits=7, decimal_places=2, blank=True, null=True)
    egresos_perifoneo             = models.DecimalField(max_digits=7, decimal_places=2, blank=True, null=True)
    egresos_cobrador              = models.DecimalField(max_digits=7, decimal_places=2, blank=True, null=True)
    total_egresos                 = models.DecimalField(max_digits=7, decimal_places=2, blank=True, null=True)
    saldo_final_caja              = models.DecimalField(max_digits=7, decimal_places=2, blank=True, null=True)
    observaciones                 = models.TextField(max_length=900, default='', blank=True, null=True)
    alerta_observaciones          = models.BooleanField(default=False)
    registro_activo               = models.BooleanField(default=True)
    capturo                       = models.CharField(max_length=100, default='')
    fecha_creacion                = models.DateTimeField(auto_now_add=True, blank=True, null=True)
    fecha_modificacion            = models.DateTimeField(auto_now=True, blank=True, null=True)
    fecha_captura                 = models.DateField(blank=True, null=True)

    class Meta:
        verbose_name        = 'Corte'
        verbose_name_plural = 'Cortes'
        #unique_together     = ['periodo_evaluado_inicio', 'periodo_evaluado_fin']

    def __str__(self):
    	return "%s" % (self.id)

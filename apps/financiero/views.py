#import os
import hashlib
import datetime
from io import BytesIO

from django.shortcuts import render, redirect
from django.http import HttpResponse, JsonResponse
from django.views.generic import View

from reportlab.pdfgen import canvas
from reportlab.lib.colors import PCMYKColor
from reportlab.lib.pagesizes import letter, legal, landscape

from .models import Corte
from apps.comite.models import Municipio, Comite
from .forms import FinancieroForm


def listFinancieroGeneral_view(request):
    # LISTADO GENERAL DE CORTES
    listFinanciero = list(Corte.objects.filter(registro_activo=True).order_by('comite_id__municipio').values('id', 'comite_id', 'comite_id__clasificacion_politica', 'comite_id__nombre_inegi', 'fecha_corte', 'folios_utilizados', 'saldo_caja', 'ingresos_porContratos', 'ingresos_porCuotasMensuales', 'ingresos_porAportaciones', 'ingresos_porOtros', 'total_ingresos', 'saldo_caja_mas_ingresos', 'total_egresos', 'saldo_final_caja', 'observaciones', 'periodo_evaluado_inicio', 'periodo_evaluado_fin'))
    if listFinanciero:
        return render(request, 'financiero/listFinancieroGeneral.html', {'listFinanciero': listFinanciero})
    else:
        return render(request, 'financiero/listFinancieroGeneral.html', {'listFinanciero': 'vacio'})


def listFinancieroComite_view(request, id):
    # LISTADO POR COMITE
    listFinanciero = list(Corte.objects.filter(comite=id, registro_activo=True).values('id', 'comite_id', 'fecha_corte', 'periodo_evaluado', 'folios_utilizados', 'saldo_caja', 'ingresos_porContratos', 'ingresos_porCuotasMensuales', 'ingresos_porAportaciones', 'ingresos_porOtros', 'total_ingresos', 'saldo_caja_mas_ingresos', 'total_egresos', 'saldo_final_caja', 'observaciones', 'capturo'))
    nombre_comite  = list(Comite.objects.filter(id=id).values('id', 'clasificacion_politica', 'nombre', 'municipio'))
    if listFinanciero:
        if nombre_comite:
            return render(request, 'financiero/listFinancieroComite.html', {'listFinanciero': listFinanciero, 'nombre_comite': nombre_comite})
        else:
            return render(request, 'financiero/listFinancieroComite.html', {'listFinanciero': 'vacio', 'nombre_comite': 'vacio'})
    else:
        if nombre_comite:
            return render(request, 'financiero/listFinancieroComite.html', {'listFinanciero': 'vacio', 'nombre_comite': nombre_comite})
        else:
            return render(request, 'financiero/listFinancieroComite.html', {'listFinanciero': 'vacio', 'nombre_comite': 'vacio'})


def listFinancieroMunicipio_view(request, id):
    # LISTADO DE CORTES POR MUNICIPIO
    listFinanciero = list(Corte.objects.filter(comite_id=id, registro_activo=True).order_by('-periodo_evaluado_inicio').values('id', 'comite_id', 'fecha_corte', 'periodo_evaluado_inicio', 'periodo_evaluado_fin', 'folios_utilizados', 'saldo_caja', 'ingresos_porContratos', 'ingresos_porCuotasMensuales', 'ingresos_porAportaciones', 'ingresos_porOtros', 'total_ingresos', 'saldo_caja_mas_ingresos', 'total_egresos', 'saldo_final_caja', 'observaciones', 'alerta_observaciones', 'capturo'))
    nombre_comite  = list(Comite.objects.filter(id=id).values('id', 'clasificacion_politica', 'nombre', 'municipio', 'municipio__nombre'))
    if listFinanciero:
        if nombre_comite:
            return render(request, 'financiero/listFinancieroMunicipio.html', {'listFinanciero': listFinanciero, 'nombre_comite': nombre_comite})
        else:
            return render(request, 'financiero/listFinancieroMunicipio.html', {'listFinanciero': 'vacio', 'nombre_comite': 'vacio'})
    else:
        if nombre_comite:
            return render(request, 'financiero/listFinancieroMunicipio.html', {'listFinanciero': 'vacio', 'nombre_comite': nombre_comite})
        else:
            return render(request, 'financiero/listFinancieroMunicipio.html', {'listFinanciero': 'vacio', 'nombre_comite': 'vacio'})            


def formFinanciero_view(request):
    # FORMULARIO PARA CAPTURA DE NUEVOS REGISTROS
    if request.method == 'POST':
        form      = FinancieroForm(request.POST)
        id_comite = request.POST['comite']
        """
        Anexo para identificar posible registro repetido
        - recibimos los datos
        - buscarmos el último registro para la tabla que nos atañe
        - compramos fechas
        """
        #inicio_periodo_evaluado = request.POST['periodo_evaluado_inicio']
        #fin_periodo_evaluado    = request.POST['periodo_evaluado_fin']
        consulta = Corte.objects.filter(comite=id_comite).last()
        print(consulta)
        comite = list(Comite.objects.filter(id=id_comite).values('id'))
        if form.is_valid():
            form.save(commit=False)
            return redirect('listFinancieroMunicipio', id=comite[0]['id'])
    else:
        form = FinancieroForm()
    return render(request, 'financiero/formFinanciero.html', {'form': form})


class BorrFinanciero_view(View):
    # BORRAR REGISTRO CON AYUDA DE AJAX
    def get(self, request):
        if request.method == 'GET':
            id_corte = request.GET['id']
            Corte.objects.filter(id=id_corte).update(registro_activo=False)
        return JsonResponse({'status': 'borrado'})


def updaFinanciero_view(request, id):
    # ACTUALIZACIÓN DE REGISTROS
    corte            = id
    corte_actualizar = Corte.objects.get(id=corte)
    comite           = list(Corte.objects.filter(id=corte).values('comite'))
    if request.method == 'POST':
        form = FinancieroForm(request.POST, instance=corte_actualizar)
        if form.is_valid():
            form.save()
            return redirect('listFinancieroMunicipio', id=comite[0]['comite'])
    else:
        form = FinancieroForm(instance=corte_actualizar)
    return render(request, 'financiero/updaFinanciero.html', {'form': form})


def buscSaldoFinal_view(request):
    # BUSCAR EL SALDO FINAL EN CAJA DEL ULTIMO REGISTRO INGRESADO PARA CADA COMITÉ
    if request.method == 'GET':
        # OBTENEMOS POR GET EL ID DEL COMITÉ Y HACEMOS UNA CONSULTA PARA SABER SI YA EXISTEN REGISTROS PREVIOS
        id_comite = request.GET['comite']
        registro_coincidente = Corte.objects.filter(comite=id_comite, registro_activo=True).order_by('comite', 'id').values('saldo_final_caja').last()
        if registro_coincidente:
            return JsonResponse({'saldo_final_caja': registro_coincidente})
        else:
            saldo_final_caja = 0
            return JsonResponse({'saldo_final_caja': saldo_final_caja})


def buscYaRegistrado_view(request):
    # BUSCAR EL SALDO FINAL EN CAJA DEL ULTIMO REGISTRO INGRESADO PARA CADA COMITÉ
    if request.method == 'GET':
        # OBTENEMOS POR GET EL ID DEL COMITÉ Y HACEMOS UNA CONSULTA PARA SABER SI YA EXISTEN REGISTROS PREVIOS
        id_comite = request.GET['comite']
        registro_coincidente = Corte.objects.filter(comite=id_comite, registro_activo=True).order_by('comite', 'id').values('fecha_corte', 'periodo_evaluado_inicio', 'periodo_evaluado_fin').last()
        if registro_coincidente:
            return JsonResponse({'registro_coincidente': registro_coincidente})
        else:
            return JsonResponse({'registro_coincidente': 'vacio'})


def encabezado_pie(c, usuario):
    # Colores
    white = PCMYKColor(0, 0, 0, 0)
    blue  = PCMYKColor(82, 45, 0, 4)
    #gray = PCMYKColor(0, 0, 0, 16)
    black = PCMYKColor(50, 0, 0, 100)
    # Firma
    hoy            = datetime.datetime.today()
    fecha          = hoy.strftime("%Y-%m-%d %H:%M:%S")
    dato_encriptar = usuario + fecha
    firma          = hashlib.md5(dato_encriptar.encode('utf-8')).hexdigest()

    # Encabezado
    c.setLineWidth(0.3)
    c.setFont('Helvetica', 13)
    c.setFillColor(black)
    c.drawImage('static/img/logo-dual.png', 770, 540, width=211, height=54)
    c.drawString(50, 560, 'COMISIÓN ESTATAL DE AGUA Y SANEAMIENTO')
    c.drawString(50, 546, 'CORTES DE CAJA')
    c.line(355, 546, 355, 572)
    c.setFont('Helvetica-Bold', 18)
    c.line(50, 540, 760, 540)

    # Pie de Página
    c.setFont('Helvetica', 8)
    c.line(50, 50, 980, 50)
    c.setFillColor(black)
    c.drawString(818, 40, 'Coordinación Estatal de Agua y Saneamiento')
    c.drawString(871, 30, 'Dirección de Desarrollo Social')
    c.drawString(846, 20, 'Evaluación Administrativa a UDESAS')
    c.setFillColor(black, alpha=0.4)
    c.drawString(50, 40, 'Firma digital: ' + firma)
    c.drawString(50, 30, 'Reporte generado por: ' + usuario)
    c.drawString(50, 20, fecha)


def repoGeneralAnterior_view(request):
    response                        = HttpResponse(content_type='application/pdf')
    response['Content-Disposition'] = 'attachment; filename=reportGral_CORTES.pdf'
    buffer                          = BytesIO()
    c                               = canvas.Canvas(buffer, pagesize=landscape(legal))
    c.setTitle('reportGral_CORTES')
    w, h                            = letter
    usuario                         = request.user.username

    # Consulta de datos para el reporte
    consulta = list(Corte.objects.filter(registro_activo=True).values('comite__nombre', 'comite__municipio', 'fecha_corte', 'periodo_evaluado_inicio', 'periodo_evaluado_fin', 'folios_utilizados', 'saldo_caja', 'ingresos_porContratos', 'ingresos_porCuotasMensuales', 'ingresos_porAportaciones', 'ingresos_porOtros', 'total_ingresos', 'saldo_caja_mas_ingresos'))
    for dato in range(len(consulta)):
        nombre_municipio = list(Municipio.objects.filter(municipio_id=consulta[dato]['comite__municipio']).values('nombre'))

    # Firma
    usuario                         = request.user.username
    hoy                             = datetime.datetime.today()
    fecha                           = hoy.strftime("%d-%m-%Y %H:%M:%S")
    dato_encriptar                  = fecha
    firma                           = hashlib.md5(dato_encriptar.encode('utf-8')).hexdigest()
    
    # Inseción del encabezado y pie de página
    encabezado_pie(c, usuario)

    # Colores
    white                           = PCMYKColor(0, 0, 0, 0)
    black                           = PCMYKColor(50, 0, 0, 100)
    blue                            = PCMYKColor(74, 38, 0, 35)
    blue2                           = PCMYKColor(61, 12, 0, 14)
    red                             = PCMYKColor(0, 100, 100, 0)

    # Contenido de la Página
    c.setFillColor(black)
    c.setFont('Helvetica', 10)
    # En la dirección se debe integrar el código postal
    c.drawString(60, 540, 'Dirección / Address: ')
    c.drawString(60, 528, 'Cédula de identidad / Passport')
    c.drawString(360, 528, 'País / Country')
    c.drawString(60, 516, 'Estado / State')
    c.drawString(260, 516, 'Ciudad / City')
    c.drawString(60, 504, 'Teléfono / Phone')
    c.drawString(260, 504, 'Correo electrónico / Email: ')
    c.drawString(60, 492, 'Placas del automóvil')
    c.drawString(60, 480, 'Observaciones / Observations')

    c.showPage()

    c.save()
    pdf = buffer.getvalue()
    buffer.close()
    response.write(pdf)
    return response


def repoGeneral_view(request):
    response                        = HttpResponse(content_type='application/pdf')
    response['Content-Disposition'] = 'attachment; filename=reportGral_CORTES.pdf'
    buffer                          = BytesIO()
    c                               = canvas.Canvas(buffer, pagesize=landscape(legal))
    c.setTitle('reportGral_CORTES.pdf')
    w, h                            = letter
    usuario                         = request.user.username

    # Consulta de datos para el reporte
    # , fecha_captura__lte='2019-12-31'
    # datos_corte2 = Corte.objects.filter(registro_activo=True, fecha_captura__gte='2019-01-01').count()
    # print(datos_corte2)
    datos_corte = list(Corte.objects.filter(registro_activo=True, fecha_captura__gte='2019-01-01').order_by('comite__municipio__nombre').values('comite__nombre', 'comite__municipio', 'comite__municipio__nombre', 'fecha_corte', 'periodo_evaluado_inicio', 'periodo_evaluado_fin', 'folios_utilizados', 'saldo_caja', 'ingresos_porContratos', 'ingresos_porCuotasMensuales', 'ingresos_porAportaciones', 'ingresos_porOtros', 'total_ingresos', 'saldo_caja_mas_ingresos', 'egresos_compensacionBombero', 'egresos_refaccionesMateriales', 'egresos_manoObra', 'egresos_energiaElectrica', 'egresos_pasajesViaticos', 'egresos_papeleria', 'egresos_telefono', 'egresos_gratificaciones', 'egresos_compraEquipo', 'egresos_otros', 'egresos_contratos', 'egresos_combustibles', 'total_egresos', 'saldo_final_caja'))

    municipio = {}
    for dato in range(len(datos_corte)):
        municipio.update({'municipio': datos_corte[dato]['comite__municipio__nombre'], 'comite': datos_corte[dato]['comite__nombre'], 'fecha de corte': datos_corte[dato]['fecha_corte'], 'inicio periodo evaluado': datos_corte[dato]['periodo_evaluado_inicio'], 'fin periodo evaluado': datos_corte[dato]['periodo_evaluado_fin']})
    print(municipio)
    # Firma
    usuario                         = request.user.username
    hoy                             = datetime.datetime.today()
    fecha                           = hoy.strftime("%d-%m-%Y %H:%M:%S")
    dato_encriptar                  = fecha
    firma                           = hashlib.md5(dato_encriptar.encode('utf-8')).hexdigest()
    
    # Inseción del encabezado y pie de página
    encabezado_pie(c, usuario)

    # Colores
    white                           = PCMYKColor(0, 0, 0, 0)
    black                           = PCMYKColor(50, 0, 0, 100)
    blue                            = PCMYKColor(74, 38, 0, 35)
    blue2                           = PCMYKColor(61, 12, 0, 14)
    green                           = PCMYKColor(84, 0, 100, 0)
    red                             = PCMYKColor(0, 100, 90, 0)

    # Contenido de la Página 1: Registros con Totales de Ingresos y Egresos
    # Encabezado de la tabla de datos
    c.setFillColor(blue)
    c.rect(50, 500, 930, 30, fill=True, stroke=False)
    c.setFillColor(white)
    c.setFont('Helvetica-Bold', 9)
    c.drawString(54, 510, 'No.')
    c.drawString(80, 510, 'FECHA DE CORTE')
    c.drawString(190, 510, 'PERIODO EVALUADO')
    c.drawString(320, 510, 'FOLIOS UTILIZADOS')
    c.drawString(440, 510, 'SDO. CAJA')
    c.drawString(520, 510, 'TOTAL DE INGRESOS')
    c.drawString(640, 510, 'SDO. CAJA + ING.')
    c.drawString(740, 510, 'TOTAL DE EGRESOS')
    c.drawString(880, 510, 'SDO. F. CAJA')

    c.setFillColor(black)
    c.setFont('Helvetica', 10)

    linea_inicial = 478
    siguiente_linea = 10

    c.drawString(80, 478, str(datos_corte[0]['comite__municipio__nombre']))
    for dato in range(len(datos_corte)):
        nombre_municipio = list(Municipio.objects.filter(municipio_id=datos_corte[dato]['comite__municipio']).values('nombre'))
        c.drawString(54, linea_inicial - dato * 12, str(dato + 1))
        c.drawString(80, linea_inicial - dato * 12, str(datos_corte[dato]['fecha_corte']))

    if datos_corte:
        registros = len(datos_corte)
        fila      = 478

        if datos_corte[0]['saldo_final_caja'] < 0:
            c.setFillColor(red)
        else:
            c.setFillColor(green)

        # ESTE CICLO SE USA PARA LA IMPRESIÓN DE LA LISTA DE FOLIOS USADOS
        i           = 0
        h           = 0
        fila_actual = 464 # A PARTIR DEL SEGUNDO REGISTRO SE INICIA LA IMPRESIÓN YA QUE EL PRIMERO SE IMPRIME FUERA DEL CICLO

        c.showPage()
        # Encabezado
        #encabezadoPie(c, w, firma, usuario, fecha, blue, white, black, datos_comite, datos_comite_clasificacion_politica)

        # Contenido de la Página 2: Desgloce de Ingresos y Egresos
        c.setFillColor(blue2)
        c.rect(50,490,240,20, fill=True, stroke=False)
        c.setFillColor(red)
        c.rect(290,490,580,20, fill=True, stroke=False)
        c.setFillColor(white)
        c.setFont('Helvetica-Bold', 10)
        c.drawString(60, 496, 'No.')
        c.drawString(90, 496, 'INGRESOS')
        c.drawString(300, 496, 'No.')
        c.drawString(330, 496, 'EGRESOS')

        c.setFont('Helvetica', 12)
        c.setFillColor(black)

        # Datos de la segunda hoja (detalles)
        # Inseción del encabezado y pie de página
        encabezado_pie(c, usuario)
        
        contador = 1
        for i in datos_corte:
            c.drawString(60, 478, str(contador))
            if i['ingresos_porContratos'] != None:
                c.drawString(90, 478, 'por Contratos: $' + str(i['ingresos_porContratos']))
            else:
                c.drawString(90, 478, 'por Contratos: ')
            if i['ingresos_porCuotasMensuales'] != None:
                c.drawString(90, 464, 'por Cuotas Mensuales: $' + str(i['ingresos_porCuotasMensuales']))
            else:
                c.drawString(90, 464, 'por Cuotas Mensuales: ')
            if i['ingresos_porAportaciones'] != None:
                c.drawString(90, 450, 'por Aportaciones: $' + str(i['ingresos_porAportaciones']))
            else:
                c.drawString(90, 450, 'por Aportaciones: ')
            if i['ingresos_porOtros'] != None:
                c.drawString(90, 436, 'por Otros: $' + str(i['ingresos_porOtros']))
            else:
                c.drawString(90, 436, 'por Otros: ')
            if i['egresos_compensacionBombero'] != None:
                c.drawString(330, 478, 'por Compensación al Bombero: $' + str(i['egresos_compensacionBombero']))
            else:
                c.drawString(330, 478, 'por Compensación al Bombero: ')
            if i['egresos_refaccionesMateriales'] != None:
                c.drawString(330, 464, 'por Compra de Refacciones y Materiales: $' + str(i['egresos_refaccionesMateriales']))
            else:
                c.drawString(330, 464, 'por Compra de Refacciones y Materiales: ')
            if i['egresos_manoObra'] != None:
                c.drawString(330, 450, 'por Mano de Obra: $' + str(i['egresos_manoObra']))
            else:
                c.drawString(330, 450, 'por Mano de Obra: ')
            if i['egresos_energiaElectrica'] != None:
                c.drawString(330, 436, 'por Pago de Energía Eléctrica: $' + str(i['egresos_energiaElectrica']))
            else:
                c.drawString(330, 436, 'por Pago de Energía Eléctrica: ')
            if i['egresos_pasajesViaticos'] != None:
                c.drawString(330, 422, 'por Pasajes y Viáticos: $' + str(i['egresos_pasajesViaticos']))
            else:
                c.drawString(330, 422, 'por Pasajes y Viáticos: ')
            if i['egresos_papeleria'] != None:
                c.drawString(330, 408, 'por Compra de Papelería: $' + str(i['egresos_papeleria']))
            else:
                c.drawString(330, 408, 'por Compra de Papelería: ')
            if i['egresos_telefono'] != None:
                c.drawString(630, 478, 'por Compra de Saldo Telefónico: $' + str(i['egresos_telefono']))
            else:
                c.drawString(630, 478, 'por Compra de Saldo Telefónico: ')
            if i['egresos_gratificaciones'] != None:
                c.drawString(630, 464, 'por Pago de Gratificaciones: $' + str(i['egresos_gratificaciones']))
            else:
                c.drawString(630, 464, 'por Pago de Gratificaciones: ')
            if i['egresos_compraEquipo'] != None:
                c.drawString(630, 450, 'por Compra de Equipo: $' + str(i['egresos_compraEquipo']))
            else:
                c.drawString(630, 450, 'por Compra de Equipo: ')
            if i['egresos_otros'] != None:
                c.drawString(630, 436, 'por Otros: $' + str(i['egresos_otros']))
            else:
                c.drawString(630, 436, 'por Otros: ')
            if i['egresos_contratos'] != None:
                c.drawString(630, 422, 'por Pago de Contratos: $' + str(i['egresos_contratos']))
            else:
                c.drawString(630, 422, 'por Pago de Contratos: ')
            if i['egresos_combustibles'] != None:
                c.drawString(630, 408, 'por Compra de Combustibles: $' + str(i['egresos_combustibles']))
            else:
                c.drawString(630, 408, 'por Compra de Combustibles: ')
            contador += 1
    else:
        c.setFillColor(black)
        c.setFont('Helvetica', 14)
        c.drawString(420, 408, 'Sin registros financieros')

    c.showPage()

    c.save()
    pdf = buffer.getvalue()
    buffer.close()
    response.write(pdf)
    return response

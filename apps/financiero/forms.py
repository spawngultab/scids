from django import forms
from .models import Corte


class FinancieroForm(forms.ModelForm):
    class Meta:
        model   = Corte
        fields  = '__all__'
        widgets = {
            'comite': forms.TextInput(attrs={'class': 'form-control'}),
            'fecha_corte': forms.TextInput(attrs={'class':'form-control', 'type':'date'}),
            'periodo_evaluado_inicio': forms.TextInput(attrs={'class':'form-control fecha_inicio', 'type':'date'}),
            'periodo_evaluado_fin': forms.TextInput(attrs={'class':'form-control fecha_fin', 'type':'date'}),
            'folios_utilizados': forms.Textarea(attrs={'class': 'form-control', 'rows': '4'}),
            'saldo_caja': forms.TextInput(attrs={'class': 'form-control'}),
            'ingresos_porContratos': forms.TextInput(attrs={'class': 'form-control'}),
            'ingresos_porCuotasMensuales': forms.TextInput(attrs={'class': 'form-control'}),
            'ingresos_porAportaciones': forms.TextInput(attrs={'class': 'form-control'}),
            'ingresos_porOtros': forms.TextInput(attrs={'class': 'form-control'}),
            'total_ingresos': forms.TextInput(attrs={'class': 'form-control'}),
            'saldo_caja_mas_ingresos': forms.TextInput(attrs={'class': 'form-control'}),
            'egresos_compensacionBombero': forms.TextInput(attrs={'class': 'form-control'}),
            'egresos_refaccionesMateriales': forms.TextInput(attrs={'class': 'form-control'}),
            'egresos_manoObra': forms.TextInput(attrs={'class': 'form-control'}),
            'egresos_energiaElectrica': forms.TextInput(attrs={'class': 'form-control'}),
            'egresos_pasajesViaticos': forms.TextInput(attrs={'class': 'form-control'}),
            'egresos_papeleria': forms.TextInput(attrs={'class': 'form-control'}),
            'egresos_telefono': forms.TextInput(attrs={'class': 'form-control'}),
            'egresos_gratificaciones': forms.TextInput(attrs={'class': 'form-control'}),
            'egresos_compraEquipo': forms.TextInput(attrs={'class': 'form-control'}),
            'egresos_contratos': forms.TextInput(attrs={'class': 'form-control'}),
            'egresos_combustibles': forms.TextInput(attrs={'class': 'form-control'}),
            'egresos_perifoneo': forms.TextInput(attrs={'class': 'form-control'}),
            'egresos_cobrador': forms.TextInput(attrs={'class': 'form-control'}),
            'egresos_otros': forms.TextInput(attrs={'class': 'form-control'}),
            'total_egresos': forms.TextInput(attrs={'class': 'form-control'}),
            'saldo_final_caja': forms.TextInput(attrs={'class': 'form-control'}),
            'observaciones': forms.Textarea(attrs={'class': 'form-control', 'rows': '4'}),
            'alerta_observaciones': forms.CheckboxInput(),
            'registro_activo': forms.CheckboxInput(attrs={'hidden': 'hidden'}),
            'capturo': forms.TextInput(attrs={'class': 'form-control', 'hidden': 'hidden'}),
            'fecha_captura': forms.TextInput(attrs={'class':'form-control', 'type':'date'}),
        }

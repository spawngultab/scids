from django.contrib import admin
from import_export import resources
from import_export.admin import ImportExportModelAdmin
from .models import Corte


class CorteResource(resources.ModelResource):
	class Meta:
		model = Corte


class CorteAdmin(ImportExportModelAdmin, admin.ModelAdmin):
	list_display    = ('comite', 'fecha_corte')
	readonly_fields = ('fecha_creacion', 'fecha_modificacion')
	resource_class  = CorteResource


admin.site.register(Corte, CorteAdmin)

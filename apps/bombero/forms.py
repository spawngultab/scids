from django import forms
from .models import Bombero


class BomberoForm(forms.ModelForm):
    class Meta:
        model   = Bombero
        fields  = '__all__'
        widgets = {
            'nombre_completo': forms.TextInput(attrs={'class': 'form-control'}),
            'sexo': forms.Select(attrs={'class': 'custom-select sexo'}),
            'telefono': forms.NumberInput(attrs={'class': 'form-control tel'}),
            'tipo_sistema': forms.Select(attrs={'class': 'custom-select'}),
            'antiguedad_anios': forms.NumberInput(attrs={'class': 'form-control'}),
            'antiguedad_meses': forms.NumberInput(attrs={'class': 'form-control'}),
            'salario_mensual': forms.NumberInput(attrs={'class': 'form-control', 'step': 0.01}),
            'meses_adeudo': forms.NumberInput(attrs={'class': 'form-control'}),
            'importe_adeudo': forms.NumberInput(attrs={'class': 'form-control', 'step': 0.01}),
            'quien_paga': forms.Select(attrs={'class': 'custom-select'}),
            'estatus_sistema': forms.Select(attrs={'class': 'custom-select'}),
            'horario_bombeo': forms.Textarea(attrs={'class': 'form-control', 'rows': '4'}),
            'observaciones': forms.Textarea(attrs={'class': 'form-control', 'rows': '4'}),
            'registro_activo': forms.CheckboxInput(attrs={'hidden': 'hidden'}),
        }

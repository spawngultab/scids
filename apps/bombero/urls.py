from django.contrib import admin
from django.contrib.auth.decorators import login_required
from django.urls import path, include
from apps.bombero import views


urlpatterns = [
    path('', login_required(views.listBombero_view), name='listBombero'),
    path('formBombero/', login_required(views.formBombero_view), name='formBombero'),
    path('updaBombero/<int:id>/', login_required(views.updaBombero_view), name='updaBombero'),
    path('regiBombero/<int:id>/', login_required(views.regiBombero_view), name='regiBombero'),
    path('borrBombero/', login_required(views.BorrBombero_view.as_view()), name='borrBombero'),
]

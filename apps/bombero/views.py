from django.shortcuts import render, redirect
from django.views.generic import View
from django.http import JsonResponse
from .models import Bombero
from .forms import BomberoForm
from apps.comite.models import Comite


def listBombero_view(request):
    # LISTADO GENERAL
    listBomberos = list(Bombero.objects.filter(registro_activo=True).values())
    if listBomberos:
        return render(request, 'bombero/listBombero.html', {'listBomberos': listBomberos})
    else:
        return render(request, 'bombero/listBombero.html', {'listBomberos': 'vacio'})


def formBombero_view(request):
    if request.method == 'POST':
        form = BomberoForm(request.POST)
        if form.is_valid():
            form.save()
            return redirect('listBombero')
    else:
        form = BomberoForm()
    return render(request, 'bombero/formBombero.html', {'form': form})


def updaBombero_view(request, id):
    bombero = id
    bombero_actualizar = Bombero.objects.get(id=bombero)
    municipio_bombero = list(Comite.objects.filter(bombero=bombero).values('municipio_id', 'municipio_id__nombre'))
    municipio_id = municipio_bombero[0]['municipio_id']
    if request.method == 'POST':
        form = BomberoForm(request.POST, instance=bombero_actualizar)
        if form.is_valid():
            form.save()
            return redirect('regiBombero', id=bombero)
    else:
        form = BomberoForm(instance=bombero_actualizar)
    return render(request, 'bombero/updaBombero.html', {'form': form, 'municipio_id': municipio_id, 'id': bombero})


def regiBombero_view(request, id):
    bombero = id
    registro_comite = list(Comite.objects.filter(bombero=bombero))
    if registro_comite:
        registro_bombero = list(Bombero.objects.filter(id=bombero).values())
        return render(request, 'bombero/regiBombero.html', {'registro_comite': registro_comite, 'registro_bombero': registro_bombero, 'comite': 'si'})
    else:
        registro_bombero = list(Bombero.objects.filter(id=bombero).values())
        return render(request, 'bombero/regiBombero.html', {'registro_bombero': registro_bombero, 'comite': 'no'})


class BorrBombero_view(View):
    # BORRAR REGISTRO CON AYUDA DE AJAX
    def get(self, request):
        if request.method == 'GET':
            id = request.GET['id']
            bombero_eliminar = Bombero.objects.filter(id=id).update(registro_activo=False)
            data = dict()
            data['respuesta'] = {'status': 'borrado'}
        return JsonResponse(data)

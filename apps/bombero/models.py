#-*- coding: utf-8 -*-
from django.db import models


class Bombero(models.Model):
    SEXO = (
        ('Femenino', 'Femenino'),
        ('Masculino', 'Masculino')
    )

    TIPO_SISTEMA = (
        ('Gal. Filtrante', 'Galería Filtrante'),
        ('Pozo Artesiano', 'Pozo Artesiano'),
        ('Pozo Jarra', 'Pozo Jarra'),
        ('Pozo Perforado', 'Pozo Perforado'),
        ('Pozo Profundo', 'Pozo Profundo'),
        ('Pozo Puyon', 'Pozo Puyon'),
        ('P.T.A.R.', 'Planta de Tratamiento de Aguas Residuales'),
        ('Pta. Pot.', 'Planta Potabilizadora'),
    )

    QUIEN_PAGA = (
        ('Ayuntamiento', 'Ayuntamiento'),
        ('C.E.A.S.', 'C.E.A.S.'),
        ('Udesa', 'Udesa'),
        ('Sub Udesa', 'Sub Udesa'),
    )

    ESTATUS_SISTEMA = (
        ('Desmantelado', 'Desmantelado'),
        ('Fuera de Servicio', 'Fuera de Servicio'),
        ('Operando', 'Operando'),
        ('Regular', 'Regular'),
    )

    nombre_completo  = models.CharField(max_length=200, default='', blank=True, null=True)
    sexo             = models.CharField(max_length=10, choices=SEXO, blank=True, null=True)
    telefono         = models.CharField(max_length=15, default='', null=True, blank=True)
    tipo_sistema     = models.CharField(max_length=60, choices=TIPO_SISTEMA, default='', blank=True, null=True)
    antiguedad_anios = models.IntegerField(default=0, blank=True, null=True)
    antiguedad_meses = models.IntegerField(default=0, blank=True, null=True)
    salario_mensual  = models.DecimalField(max_digits=7, decimal_places=2, blank=True, null=True)
    meses_adeudo     = models.IntegerField(blank=True, null=True)
    importe_adeudo   = models.DecimalField(max_digits=7, decimal_places=2, blank=True, null=True)
    quien_paga       = models.CharField(max_length=15, choices=QUIEN_PAGA, default='', blank=True, null=True)
    estatus_sistema  = models.CharField(max_length=20, choices=ESTATUS_SISTEMA, default='', blank=True, null=True)
    horario_bombeo   = models.CharField(max_length=300, default='', blank=True, null=True)
    observaciones    = models.TextField(max_length=900, default='', blank=True, null=True)
    registro_activo  = models.BooleanField(default=True)

    class Meta:
        verbose_name        = 'Bombero'
        verbose_name_plural = 'Bomberos'

    def __str__(self):
        return self.nombre_completo

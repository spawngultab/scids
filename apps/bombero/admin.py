from django.contrib import admin
from import_export import resources
from import_export.admin import ImportExportModelAdmin
from .models import Bombero


class BomberoResource(resources.ModelResource):
	class Meta:
		model = Bombero


class BomberoAdmin(ImportExportModelAdmin, admin.ModelAdmin):
	list_display   = ('nombre_completo', 'tipo_sistema', 'importe_adeudo')
	resource_class = BomberoResource


admin.site.register(Bombero, BomberoAdmin)

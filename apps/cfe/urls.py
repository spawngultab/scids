from django.contrib import admin
from django.contrib.auth.decorators import login_required
from django.urls import path, include
from apps.cfe import views


urlpatterns = [
    path('', login_required(views.listCfe_view), name='listCfe'),
    path('listCfeComite/<int:id>/', views.listCfeComite_view, name='listCfeComite'),
    #path('listCfe/municipio/<int:id>/', views.listCfeMunicipio_view, name='listCfeMunicipio'),
    path('formCfe/', login_required(views.formCfe_view), name='formCfe'),
    path('updaCfe/<int:id>/', login_required(views.updaCfe_view), name='updaCfe'),
    path('borrCfe/', login_required(views.BorrCfe_view.as_view()), name='borrCfe'),
    path('buscComiteCfe/', login_required(views.buscComiteCfe_view), name='buscComiteCfe'),
    path('buscMunicipio/', login_required(views.buscMunicipio_view), name='buscMunicipio'),
    path('buscNombreComiteCfe/', login_required(views.buscNombreComiteCfe_view), name='buscNombreComiteCfe'),
]
# Generated by Django 3.0.2 on 2020-01-24 16:32

from django.db import migrations, models


class Migration(migrations.Migration):

    initial = True

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='Cfe',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('comite', models.IntegerField(blank=True, null=True)),
                ('pagado_por', models.CharField(blank=True, choices=[('Ayuntamiento', 'Ayuntamiento'), ('C.E.A.S.', 'C.E.A.S.'), ('Udesa', 'Udesa')], default='', max_length=15, null=True)),
                ('importe_mensual_aprox', models.DecimalField(blank=True, decimal_places=2, max_digits=7, null=True)),
                ('numero_medidor', models.CharField(blank=True, default='', max_length=15, null=True)),
                ('rpu', models.CharField(blank=True, default='', max_length=15, null=True)),
                ('importe_adeudo', models.DecimalField(blank=True, decimal_places=2, max_digits=7, null=True)),
                ('registro_activo', models.BooleanField(default=True)),
            ],
            options={
                'verbose_name': 'Cfe',
                'verbose_name_plural': 'Cfe',
            },
        ),
    ]

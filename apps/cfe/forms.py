from django import forms
from .models import Cfe


class CfeForm(forms.ModelForm):
    class Meta:
        model   = Cfe
        fields  = '__all__'
        widgets = {
            'comite': forms.TextInput(attrs={'class': 'form-control'}),
            'pagado_por': forms.Select(attrs={'class': 'custom-select'}),
            'importe_mensual_aprox': forms.NumberInput(attrs={'class': 'form-control', 'step': 0.01}),
            'numero_medidor': forms.TextInput(attrs={'class': 'form-control'}),
            'rpu': forms.TextInput(attrs={'class': 'form-control'}),
            'importe_adeudo': forms.NumberInput(attrs={'class': 'form-control', 'step': 0.01}),
            'registro_activo': forms.CheckboxInput(attrs={'hidden': 'hidden'}),
        }

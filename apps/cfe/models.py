from django.db import models
from apps.comite.models import Comite


class Cfe(models.Model):
    QUIEN_PAGA = (
        ('Ayuntamiento', 'Ayuntamiento'),
        ('C.E.A.S.', 'C.E.A.S.'),
        ('Udesa', 'Udesa'),
    )

    comite                = models.IntegerField(blank=True, null=True)
    pagado_por            = models.CharField(max_length=15, choices=QUIEN_PAGA, default='', blank=True, null=True)
    importe_mensual_aprox = models.DecimalField(max_digits=7, decimal_places=2, blank=True, null=True)
    numero_medidor        = models.CharField(max_length=15, default='', blank=True, null=True)
    rpu                   = models.CharField(max_length=15, default='', blank=True, null=True)
    importe_adeudo        = models.DecimalField(max_digits=7, decimal_places=2, blank=True, null=True)
    fecha_creacion        = models.DateTimeField(auto_now_add=True, blank=True, null=True)
    fecha_modificacion    = models.DateTimeField(auto_now=True, blank=True, null=True)
    registro_activo       = models.BooleanField(default=True)

    class Meta:
        verbose_name        = 'Cfe'
        verbose_name_plural = 'Cfe'

    def __str__(self):
        return self.numero_medidor

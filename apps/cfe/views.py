from django.shortcuts import render, redirect
from django.http import JsonResponse
from django.views.generic import View
from .models import Cfe
from .forms import CfeForm
from apps.comite.models import Municipio, Comite
from apps.financiero.models import Corte


def listCfe_view(request):
    listCfe = list(Cfe.objects.filter(registro_activo=True).order_by('comite').values('id', 'comite', 'pagado_por', 'importe_mensual_aprox', 'numero_medidor', 'rpu', 'importe_adeudo'))
    if listCfe:
        return render(request, 'cfe/listCfe.html', {'listCfe': listCfe})
    else:
        return render(request, 'cfe/listCfe.html', {'listCfe': 'vacio'})


def listCfeComite_view(request, id):
    listCfe = list(Cfe.objects.filter(comite=id, registro_activo=True).values('id', 'comite', 'pagado_por', 'importe_mensual_aprox', 'numero_medidor', 'rpu', 'importe_adeudo'))
    nombre_comite = list(Comite.objects.filter(id=id).values('clasificacion_politica', 'nombre', 'municipio'))
    if listCfe:
        if nombre_comite:
            return render(request, 'cfe/listCfe.html', {'listCfe': listCfe, 'nombre_comite': nombre_comite})
        else:
            return render(request, 'cfe/listCfe.html', {'listCfe': 'vacio', 'nombre_comite': 'vacio'})
    else:
        if nombre_comite:
            return render(request, 'cfe/listCfe.html', {'listCfe': 'vacio', 'nombre_comite': nombre_comite})
        else:
            return render(request, 'cfe/listCfe.html', {'listCfe': 'vacio', 'nombre_comite': 'vacio'})


def formCfe_view(request):
    if request.method == 'POST':
        form = CfeForm(request.POST)
        if form.is_valid():
            form.save()
            return redirect('listCfe')
    else:
        form = CfeForm()
    return render(request, 'cfe/formCfe.html', {'form': form})


def updaCfe_view(request, id):
    id_cfe = id
    cfe_actualizar = Cfe.objects.get(id=id_cfe)
    if request.method == 'POST':
        form = CfeForm(request.POST, instance=cfe_actualizar)
        if form.is_valid():
            form.save()
            return redirect('listCfe')
    else:
        form = CfeForm(instance=cfe_actualizar)
    return render(request, 'cfe/updaCfe.html', {'form': form})


class BorrCfe_view(View):
    # BORRAR REGISTRO CON AYUDA DE AJAX
    def get(self, request):
        if request.method == 'GET':
            id_registro = request.GET['id']
            cfe_eliminar = Cfe.objects.filter(id=id_registro).update(registro_activo=False)
        return JsonResponse({'status': 'borrado'})


def buscComiteCfe_view(request):
    if request.method == 'GET':
        dato = request.GET['municipio']
        if dato != '':
            dato_comites = list(Comite.objects.filter(municipio=dato).values('id', 'clasificacion_politica', 'nombre'))
            if dato_comites:
                data = dict()
                data = {'encontrado': 'si', 'respuesta': dato_comites}
                return JsonResponse(data)
            else:
                data = {'encontrado': 'no', 'respuesta': ''}
                return JsonResponse(data)


def buscMunicipio_view(request):
    if request.method == 'GET':
        dato = request.GET['opcion']
        if dato == 'buscar':
            dato_municipios = list(Municipio.objects.all().order_by('nombre').values('municipio_id', 'nombre'))
            data = dict()
            data = {'municipios': dato_municipios}
        return JsonResponse(data)


def buscNombreComiteCfe_view(request):
    if request.method == 'GET':
        dato = request.GET['comite']
        dato_corte = list(Corte.objects.filter(comite_id=dato).values('fecha_corte', 'periodo_evaluado_inicio', 'periodo_evaluado_fin'))
        if dato_corte:
            dato_comite = list(Comite.objects.filter(id=dato).values('id', 'clasificacion_politica', 'nombre', 'municipio_id__nombre'))
            data = dict()
            data = {'encontrado': 'si', 'respuesta': dato_comite, 'cortes': dato_corte}
            return JsonResponse(data)
        else:
            dato_comite = list(Comite.objects.filter(id=dato).values('id', 'clasificacion_politica', 'nombre', 'municipio_id__nombre'))
            data = {'encontrado': 'no', 'respuesta': dato_comite, 'cortes': ''}
            return JsonResponse(data)

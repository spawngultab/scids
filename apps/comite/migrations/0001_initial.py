# Generated by Django 3.0.2 on 2020-02-10 17:14

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    initial = True

    dependencies = [
        ('bombero', '0001_initial'),
    ]

    operations = [
        migrations.CreateModel(
            name='Municipio',
            fields=[
                ('municipio_id', models.IntegerField(default=0, primary_key=True, serialize=False, verbose_name='Número de identificación del municipio')),
                ('nombre', models.CharField(choices=[('Cárdenas', 'Cárdenas'), ('Centla', 'Centla'), ('Comalcalco', 'Comalcalco'), ('Cunduacán', 'Cunduacán'), ('Emiliano Zapata', 'Emiliano Zapata'), ('Huimanguillo', 'Huimanguillo'), ('Jalapa', 'Jalapa'), ('Jalpa de Méndez', 'Jalpa de Méndez'), ('Nacajuca', 'Nacajuca'), ('Paraíso', 'Paraíso'), ('Tacotalpa', 'Tacotalpa'), ('Teapa', 'Teapa'), ('Tenosique', 'Tenosique'), ('Zona Costera', 'Zona Costera')], max_length=20)),
                ('latitud_municipio', models.CharField(blank=True, default='', max_length=16, null=True, verbose_name='Latitud')),
                ('longitud_municipio', models.CharField(blank=True, default='', max_length=16, null=True, verbose_name='Longitud')),
            ],
            options={
                'verbose_name': 'Municipio',
                'verbose_name_plural': 'Municipios',
            },
        ),
        migrations.CreateModel(
            name='Comite',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('clasificacion_politica', models.CharField(blank=True, choices=[('Col.', 'Col.'), ('Cong.', 'Cong.'), ('Ej.', 'Ej.'), ('Fracc.', 'Fracc.'), ('Pob.', 'Pob.'), ('Ría.', 'Ría.'), ('Villa', 'Villa')], default='', max_length=6, null=True, verbose_name='Clasificación política')),
                ('nombre', models.CharField(default='', max_length=180)),
                ('nombre_inegi', models.CharField(blank=True, default='', max_length=180, null=True)),
                ('quien_administra', models.CharField(blank=True, choices=[('Ayuntamiento', 'Ayuntamiento'), ('C.E.A.S.', 'C.E.A.S.'), ('C.E.A.S./Ayuntamiento', 'C.E.A.S./Ayuntamiento'), ('Induvitab', 'Induvitab')], max_length=24, null=True, verbose_name='Quién administra el sistema')),
                ('latitud', models.CharField(blank=True, default='', max_length=24, null=True)),
                ('longitud', models.CharField(blank=True, default='', max_length=24, null=True)),
                ('tipo_comite', models.CharField(blank=True, choices=[('Udesa', 'Udesa'), ('Sin Información', 'Sin Información'), ('Sub Udesa', 'Sub Udesa')], default='Sin Información', max_length=16, null=True, verbose_name='Tipo de comité')),
                ('abastecido_por', models.IntegerField(blank=True, null=True)),
                ('calificacion_comite', models.CharField(blank=True, choices=[('Bueno', 'Bueno'), ('Malo', 'Malo'), ('Regular', 'Regular')], max_length=7, null=True, verbose_name='Calificación del comité')),
                ('tiempo_funcion_comite_mes', models.IntegerField(blank=True, null=True, verbose_name='Tiempo en función del comité (Meses)')),
                ('tiempo_funcion_comite_anios', models.IntegerField(blank=True, null=True, verbose_name='Tiempo en función del comité (Años)')),
                ('numero_habitantes', models.IntegerField(blank=True, null=True, verbose_name='Número de habitantes')),
                ('poblacion_inegi', models.IntegerField(blank=True, null=True, verbose_name='Número de habitantes INEGI')),
                ('numero_tomas_instaladas', models.IntegerField(blank=True, null=True, verbose_name='Número de tomas instaladas')),
                ('numero_tomas_domiciliarias', models.IntegerField(blank=True, null=True, verbose_name='Número de tomas domiciliarias')),
                ('cuota_mensual_toma_domiciliaria', models.DecimalField(blank=True, decimal_places=2, max_digits=7, null=True, verbose_name='Cuota mensual de toma domiciliaria ($)')),
                ('numero_tomas_comerciales', models.IntegerField(blank=True, null=True, verbose_name='Número de tomas comerciales')),
                ('cuota_mensual_toma_comercial', models.DecimalField(blank=True, decimal_places=2, max_digits=7, null=True, verbose_name='Cuota mensual de toma comercial ($)')),
                ('numero_tomas_ganaderas', models.IntegerField(blank=True, null=True, verbose_name='Número de tomas ganaderas')),
                ('cuota_mensual_toma_ganadera', models.DecimalField(blank=True, decimal_places=2, max_digits=7, null=True, verbose_name='Cuota mensual de toma ganadera ($)')),
                ('promotor', models.CharField(blank=True, default='', max_length=200, null=True)),
                ('sexo_promotor', models.CharField(blank=True, choices=[('Femenino', 'Femenino'), ('Masculino', 'Masculino')], max_length=10, null=True, verbose_name='Sexo del promotor')),
                ('telefono_promotor', models.CharField(blank=True, default='', max_length=15, null=True, verbose_name='Teléfono del promotor')),
                ('presidente', models.CharField(blank=True, default='', max_length=200, null=True)),
                ('sexo_presidente', models.CharField(blank=True, choices=[('Femenino', 'Femenino'), ('Masculino', 'Masculino')], max_length=10, null=True, verbose_name='Sexo del presidente')),
                ('telefono_presidente', models.CharField(blank=True, default='', max_length=15, null=True, verbose_name='Teléfono del presidente')),
                ('secretario', models.CharField(blank=True, default='', max_length=200, null=True)),
                ('sexo_secretario', models.CharField(blank=True, choices=[('Femenino', 'Femenino'), ('Masculino', 'Masculino')], max_length=10, null=True, verbose_name='Sexo del secretario')),
                ('telefono_secretario', models.CharField(blank=True, default='', max_length=15, null=True, verbose_name='Teléfono del secretario')),
                ('tesorero', models.CharField(blank=True, default='', max_length=200, null=True)),
                ('sexo_tesorero', models.CharField(blank=True, choices=[('Femenino', 'Femenino'), ('Masculino', 'Masculino')], max_length=10, null=True, verbose_name='Sexo del tesorero')),
                ('telefono_tesorero', models.CharField(blank=True, default='', max_length=15, null=True, verbose_name='Teléfono del tesorero')),
                ('primer_vocal', models.CharField(blank=True, default='', max_length=200, null=True)),
                ('sexo_primer_vocal', models.CharField(blank=True, choices=[('Femenino', 'Femenino'), ('Masculino', 'Masculino')], max_length=10, null=True, verbose_name='Sexo del primer vocal')),
                ('telefono_primer_vocal', models.CharField(blank=True, default='', max_length=15, null=True, verbose_name='Teléfono del primer vocal')),
                ('segundo_vocal', models.CharField(blank=True, default='', max_length=200, null=True)),
                ('sexo_segundo_vocal', models.CharField(blank=True, choices=[('Femenino', 'Femenino'), ('Masculino', 'Masculino')], max_length=10, null=True, verbose_name='Sexo del segundo vocal')),
                ('telefono_segundo_vocal', models.CharField(blank=True, default='', max_length=15, null=True, verbose_name='Teléfono del segundo vocal')),
                ('tercer_vocal', models.CharField(blank=True, default='', max_length=200, null=True)),
                ('sexo_tercer_vocal', models.CharField(blank=True, choices=[('Femenino', 'Femenino'), ('Masculino', 'Masculino')], max_length=10, null=True, verbose_name='Sexo del tercer vocal')),
                ('telefono_tercer_vocal', models.CharField(blank=True, default='', max_length=15, null=True, verbose_name='Teléfono del tercer vocal')),
                ('fontanero', models.CharField(blank=True, default='', max_length=200, null=True)),
                ('sexo_fontanero', models.CharField(blank=True, choices=[('Femenino', 'Femenino'), ('Masculino', 'Masculino')], max_length=10, null=True, verbose_name='Sexo del fontanero')),
                ('telefono_fontanero', models.CharField(blank=True, default='', max_length=15, null=True, verbose_name='Teléfono del fontanero')),
                ('delegado_municipal', models.CharField(blank=True, default='', max_length=200, null=True)),
                ('sexo_delegado_municipal', models.CharField(blank=True, choices=[('Femenino', 'Femenino'), ('Masculino', 'Masculino')], max_length=10, null=True, verbose_name='Sexo del delegado municipal')),
                ('telefono_delegado_municipal', models.CharField(blank=True, default='', max_length=15, null=True, verbose_name='Teléfono del delegado municipal')),
                ('comisariado_ejidal', models.CharField(blank=True, default='', max_length=200, null=True)),
                ('sexo_comisariado_ejidal', models.CharField(blank=True, choices=[('Femenino', 'Femenino'), ('Masculino', 'Masculino')], max_length=10, null=True, verbose_name='Sexo del comisariado ejidal')),
                ('telefono_comisariado_ejidal', models.CharField(blank=True, default='', max_length=15, null=True, verbose_name='Teléfono del comisariado ejidal')),
                ('jefe_sector', models.CharField(blank=True, default='', max_length=200, null=True)),
                ('sexo_jefe_sector', models.CharField(blank=True, choices=[('Femenino', 'Femenino'), ('Masculino', 'Masculino')], max_length=10, null=True, verbose_name='Sexo del jefe de sector')),
                ('telefono_jefe_sector', models.CharField(blank=True, default='', max_length=15, null=True, verbose_name='Teléfono del jefe de sector')),
                ('observaciones', models.TextField(blank=True, default='', max_length=900, null=True)),
                ('reestructurada', models.BooleanField(default=False)),
                ('fecha_reestructuracion', models.TextField(blank=True, max_length=10, null=True)),
                ('fecha_creacion', models.DateTimeField(auto_now_add=True, null=True)),
                ('fecha_modificacion', models.DateTimeField(auto_now=True, null=True)),
                ('registro_activo', models.BooleanField(default=True)),
                ('bombero', models.ForeignKey(blank=True, default=0, null=True, on_delete=django.db.models.deletion.CASCADE, to='bombero.Bombero')),
                ('municipio', models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.CASCADE, to='comite.Municipio')),
            ],
            options={
                'verbose_name': 'Comité',
                'verbose_name_plural': 'Comités',
            },
        ),
        migrations.CreateModel(
            name='Administrador',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('titulo', models.CharField(blank=True, choices=[('Arq.', 'Arq.'), ('Biól.', 'Biól.'), ('C.', 'C.'), ('Ing.', 'Ing.'), ('Lic.', 'Lic.'), ('Téc.', 'Téc.')], default='Título', max_length=6, null=True, verbose_name='Título')),
                ('nombre', models.CharField(default='', max_length=80, verbose_name='Nombre(s)')),
                ('apellido_paterno', models.CharField(default='', max_length=70, verbose_name='Apellido paterno')),
                ('apellido_materno', models.CharField(blank=True, default='', max_length=70, null=True, verbose_name='Apellido materno')),
                ('sexo', models.CharField(blank=True, choices=[('Femenino', 'Femenino'), ('Masculino', 'Masculino')], default='Sexo', max_length=10, null=True)),
                ('telefono_personal', models.TextField(blank=True, default='', max_length=300, null=True, verbose_name='Teléfono personal')),
                ('direccion', models.CharField(blank=True, default='', max_length=200, null=True, verbose_name='Dirección')),
                ('telefono_oficina', models.TextField(blank=True, default='', max_length=300, null=True, verbose_name='Teléfono de la oficina')),
                ('correo_electronico', models.EmailField(blank=True, default='', max_length=254, null=True, verbose_name='Correo eléctronico')),
                ('observaciones', models.TextField(blank=True, default='', max_length=500, null=True, verbose_name='Observaciones')),
                ('municipio', models.ForeignKey(blank=True, default='', null=True, on_delete=django.db.models.deletion.CASCADE, to='comite.Municipio')),
            ],
            options={
                'verbose_name': 'Administrador',
                'verbose_name_plural': 'Administradores',
            },
        ),
    ]

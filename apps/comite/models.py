#-*- coding: utf-8 -*-
from django.db import models
from apps.bombero.models import Bombero
#from apps.financiero.models import Corte


class Municipio(models.Model):
    MUNICIPIOS = (
        ('Cárdenas', 'Cárdenas'),
        ('Centla', 'Centla'),
        ('Comalcalco', 'Comalcalco'),
        ('Cunduacán', 'Cunduacán'),
        ('Emiliano Zapata', 'Emiliano Zapata'),
        ('Huimanguillo', 'Huimanguillo'),
        ('Jalapa', 'Jalapa'),
        ('Jalpa de Méndez', 'Jalpa de Méndez'),
        ('Nacajuca', 'Nacajuca'),
        ('Paraíso', 'Paraíso'),
        ('Tacotalpa', 'Tacotalpa'),
        ('Teapa', 'Teapa'),
        ('Tenosique', 'Tenosique'),
        ('Zona Costera', 'Zona Costera')
    )

    municipio_id       = models.IntegerField('Número de identificación del municipio', default=0, primary_key=True)
    nombre             = models.CharField(max_length=20, choices=MUNICIPIOS)
    latitud_municipio  = models.CharField('Latitud', max_length=16, default='', blank=True, null=True)
    longitud_municipio = models.CharField('Longitud', max_length=16, default='', blank=True, null=True)

    class Meta:
        verbose_name        = 'Municipio'
        verbose_name_plural = 'Municipios'

    def __str__(self):
        return self.nombre


class Administrador(models.Model):
    TITULO = (
        ('Arq.', 'Arq.'),
        ('Biól.', 'Biól.'),
        ('C.', 'C.'),
        ('Ing.', 'Ing.'),
        ('Lic.', 'Lic.'),
        ('Téc.', 'Téc.')
    )

    SEXO = (
        ('Femenino', 'Femenino'),
        ('Masculino', 'Masculino')
    )

    titulo             = models.CharField('Título', max_length=6, choices=TITULO, default='Título', null=True, blank=True)
    nombre             = models.CharField('Nombre(s)', max_length=80, default='')
    apellido_paterno   = models.CharField('Apellido paterno', max_length=70, default='')
    apellido_materno   = models.CharField('Apellido materno', max_length=70, default='', null=True, blank=True)
    sexo               = models.CharField(max_length=10, choices=SEXO, default='Sexo', null=True, blank=True)
    telefono_personal  = models.TextField('Teléfono personal', max_length=300, default='', null=True, blank=True)
    municipio          = models.ForeignKey(Municipio, on_delete=models.CASCADE, default='', blank=True, null=True)
    direccion          = models.CharField('Dirección', max_length=200, default='', blank=True, null=True)
    telefono_oficina   = models.TextField('Teléfono de la oficina', max_length=300, default='', blank=True, null=True)
    correo_electronico = models.EmailField('Correo eléctronico', default='', blank=True, null=True)
    observaciones      = models.TextField('Observaciones', max_length=500, default='', blank=True, null=True)

    class Meta:
        verbose_name        = 'Administrador'
        verbose_name_plural = 'Administradores'

    def __str__(self):
        return '%s %s %s %s - Administrador de %s' % (self.titulo, self.nombre, self.apellido_paterno, self.apellido_materno, self.municipio)


class Comite(models.Model):
    """
        Implementar función que copie el registro completo del comite antes de actualizar los datos de los
        nuevos integrantes cuando haya reestructuración del comité
    """
    CLASIFICACION = (
        ('Col.', 'Col.'),
        ('Col. Agríc.', 'Col. Agríc.'),
        ('Col. Agrop.', 'Col. Agrop.'),
        ('Cong.', 'Cong.'),
        ('Ej.', 'Ej.'),
        ('Fracc.', 'Fracc.'),
        ('Indef.', 'Indef.'),
        ('Pob.', 'Pob.'),
        ('Ría.', 'Ría.'),
        ('Villa', 'Villa'),
    )

    QUIEN_ADMIN = (
        ('Ayuntamiento', 'Ayuntamiento'),
        ('C.E.A.S.', 'C.E.A.S.'),
        ('C.E.A.S./Ayuntamiento', 'C.E.A.S./Ayuntamiento'),
        ('Induvitab', 'Induvitab')
    )

    TIPO_COMITE = (
        ('Udesa', 'Udesa'),
        ('Sin Información', 'Sin Información'),
        ('Sub Udesa', 'Sub Udesa')
    )

    CALIFICACION_COMITE = (
        ('Bueno', 'Bueno'),
        ('Malo', 'Malo'),
        ('Regular', 'Regular')
    )

    SEXO = (
        ('Femenino', 'Femenino'),
        ('Masculino', 'Masculino')
    )

    clave_ceas                      = models.CharField(max_length=12, default='', blank=True, null=True)
    clasificacion_politica          = models.CharField('Clasificación política', max_length=12, choices=CLASIFICACION, default='', blank=True, null=True)
    nombre                          = models.CharField(max_length=180, default='')
    nombre_inegi                    = models.CharField(max_length=180, default='', blank=True, null=True)
    clave_inegi                     = models.CharField(max_length=14, default='', blank=True, null=True)
    quien_administra                = models.CharField('Quién administra el sistema', max_length=24, choices=QUIEN_ADMIN, blank=True, null=True)
    latitud                         = models.CharField(max_length=24, default='', blank=True, null=True)
    longitud                        = models.CharField(max_length=24, default='', blank=True, null=True)
    municipio                       = models.ForeignKey(Municipio, on_delete=models.CASCADE, blank=True, null=True)
    tipo_comite                     = models.CharField('Tipo de comité', max_length=16, choices=TIPO_COMITE, default='', blank=True, null=True)
    abastecido_por                  = models.IntegerField(null=True, blank=True)
    calificacion_comite             = models.CharField('Calificación del comité', max_length=7, choices=CALIFICACION_COMITE, blank=True, null=True)
    tiempo_funcion_comite_mes       = models.IntegerField('Tiempo en función del comité (Meses)', null=True, blank=True)
    tiempo_funcion_comite_anios     = models.IntegerField('Tiempo en función del comité (Años)', null=True, blank=True)
    numero_habitantes               = models.IntegerField('Número de habitantes', null=True, blank=True)
    poblacion_inegi                 = models.IntegerField('Número de habitantes INEGI', null=True, blank=True)
    numero_tomas_instaladas         = models.IntegerField('Número de tomas instaladas', null=True, blank=True)
    numero_tomas_domiciliarias      = models.IntegerField('Número de tomas domiciliarias', null=True, blank=True)
    cuota_mensual_toma_domiciliaria = models.DecimalField('Cuota mensual de toma domiciliaria ($)', max_digits=7, decimal_places=2, blank=True, null=True)
    numero_tomas_comerciales        = models.IntegerField('Número de tomas comerciales', blank=True, null=True)
    cuota_mensual_toma_comercial    = models.DecimalField('Cuota mensual de toma comercial ($)', max_digits=7, decimal_places=2, blank=True, null=True)
    numero_tomas_ganaderas          = models.IntegerField('Número de tomas ganaderas', blank=True, null=True)
    cuota_mensual_toma_ganadera     = models.DecimalField('Cuota mensual de toma ganadera ($)', max_digits=7, decimal_places=2, blank=True, null=True)
    promotor                        = models.CharField(max_length=200, default='', null=True, blank=True)
    sexo_promotor                   = models.CharField('Sexo del promotor', max_length=10, choices=SEXO, blank=True, null=True)
    telefono_promotor               = models.CharField('Teléfono del promotor', max_length=15, default='', null=True, blank=True)
    presidente                      = models.CharField(max_length=200, default='', null=True, blank=True)
    sexo_presidente                 = models.CharField('Sexo del presidente', max_length=10, choices=SEXO, blank=True, null=True)
    telefono_presidente             = models.CharField('Teléfono del presidente', max_length=15, default='', null=True, blank=True)
    secretario                      = models.CharField(max_length=200, default='', null=True, blank=True)
    sexo_secretario                 = models.CharField('Sexo del secretario', max_length=10, choices=SEXO, blank=True, null=True)
    telefono_secretario             = models.CharField('Teléfono del secretario', max_length=15, default='', null=True, blank=True)
    tesorero                        = models.CharField(max_length=200, default='', null=True, blank=True)
    sexo_tesorero                   = models.CharField('Sexo del tesorero', max_length=10, choices=SEXO, blank=True, null=True)
    telefono_tesorero               = models.CharField('Teléfono del tesorero', max_length=15, default='', null=True, blank=True)
    primer_vocal                    = models.CharField(max_length=200, default='', blank=True, null=True)
    sexo_primer_vocal               = models.CharField('Sexo del primer vocal', max_length=10, choices=SEXO, blank=True, null=True)
    telefono_primer_vocal           = models.CharField('Teléfono del primer vocal', max_length=15, default='', blank=True, null=True)
    segundo_vocal                   = models.CharField(max_length=200, default='', blank=True, null=True)
    sexo_segundo_vocal              = models.CharField('Sexo del segundo vocal', max_length=10, choices=SEXO, blank=True, null=True)
    telefono_segundo_vocal          = models.CharField('Teléfono del segundo vocal', max_length=15, default='', blank=True, null=True)
    tercer_vocal                    = models.CharField(max_length=200, default='', blank=True, null=True)
    sexo_tercer_vocal               = models.CharField('Sexo del tercer vocal', max_length=10, choices=SEXO, blank=True, null=True)
    telefono_tercer_vocal           = models.CharField('Teléfono del tercer vocal', max_length=15, default='', blank=True, null=True)
    bombero                         = models.ForeignKey(Bombero, on_delete=models.CASCADE, blank=True, null=True)
    fontanero                       = models.CharField(max_length=200, default='', null=True, blank=True)
    sexo_fontanero                  = models.CharField('Sexo del fontanero', max_length=10, choices=SEXO, blank=True, null=True)
    telefono_fontanero              = models.CharField('Teléfono del fontanero', max_length=15, default='', null=True, blank=True)
    delegado_municipal              = models.CharField(max_length=200, default='', null=True, blank=True)
    sexo_delegado_municipal         = models.CharField('Sexo del delegado municipal', max_length=10, choices=SEXO, blank=True, null=True)
    telefono_delegado_municipal     = models.CharField('Teléfono del delegado municipal', max_length=15, default='', null=True, blank=True)
    comisariado_ejidal              = models.CharField(max_length=200, default='', null=True, blank=True)
    sexo_comisariado_ejidal         = models.CharField('Sexo del comisariado ejidal', max_length=10, choices=SEXO, blank=True, null=True)
    telefono_comisariado_ejidal     = models.CharField('Teléfono del comisariado ejidal', max_length=15, default='', null=True, blank=True)
    jefe_sector                     = models.CharField(max_length=200, default='', null=True, blank=True)
    sexo_jefe_sector                = models.CharField('Sexo del jefe de sector', max_length=10, choices=SEXO, blank=True, null=True)
    telefono_jefe_sector            = models.CharField('Teléfono del jefe de sector', max_length=15, default='', null=True, blank=True)
    observaciones                   = models.TextField(max_length=900, default='', blank=True, null=True)
    reestructurada                  = models.BooleanField(default=False)
    fecha_reestructuracion          = models.TextField(max_length=10, default='',blank=True, null=True)
    fecha_creacion                  = models.DateTimeField(auto_now_add=True, blank=True, null=True)
    fecha_modificacion              = models.DateTimeField(auto_now=True, blank=True, null=True)
    registro_activo                 = models.BooleanField(default=True)

    class Meta:
        verbose_name        = 'Comité'
        verbose_name_plural = 'Comités'

    def __str__(self):
        return '%s %s' % (self.clasificacion_politica, self.nombre)

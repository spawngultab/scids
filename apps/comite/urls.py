from django.contrib import admin
from django.contrib.auth.decorators import login_required
from django.urls import path, include
from apps.comite import views


urlpatterns = [
    path('', login_required(views.listComiteGeneral_view), name='listComiteGeneral'),
    path('obteComiteGeneral/', login_required(views.obteComiteGeneral_view), name='obteComiteGeneral'),
    path('listComiteMunicipio/<int:municipio_id>/', login_required(views.listComiteMunicipio_view), name='listComiteMunicipio'),
    path('buscMunicipio/', login_required(views.buscMunicipio_view), name='buscMunicipio'),
    path('formComiteGeneral/', login_required(views.formComiteGeneral_view), name='formComiteGeneral'),
    #path('formComiteMunicipio/', login_required(views.formComite_view), name='formComiteMunicipio'),
    path('repoGeneralComite/', login_required(views.repoGeneralComite_view), name='repoGeneralComite'),
    path('updaComite/<int:id>/', login_required(views.updaComite_view), name='updaComite'),
    path('regiComite/<int:id>/', login_required(views.regiComite_view), name='regiComite'),
    path('geneMapa/', login_required(views.geneMapa_view), name='geneMapa'),
    path('borrComite/', login_required(views.BorrComite_view.as_view()), name='borrComite'),
    path('buscComiteUpdate/', login_required(views.buscComiteUpdate_view), name='buscComiteUpdate'),
]
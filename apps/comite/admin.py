from django.contrib import admin
from import_export import resources
from import_export.admin import ImportExportModelAdmin
from .models import Municipio, Administrador, Comite


class MunicipioResource(resources.ModelResource):
	class Meta:
		model = Municipio
		exclude = ('id',)
		import_id_fields = ('municipio_id','nombre','latitud_municipio', 'longitud_municipio')


class MunicipioAdmin(ImportExportModelAdmin, admin.ModelAdmin):
	list_display   = ('nombre',)
	resource_class = MunicipioResource


class AdministradorResource(resources.ModelResource):
	class Meta:
		model = Administrador


class AdministradorAdmin(ImportExportModelAdmin, admin.ModelAdmin):
	list_display   = ('correo_electronico', 'titulo', 'nombre', 'apellido_paterno', 'apellido_materno')
	resource_class = AdministradorResource


class ComiteResource(resources.ModelResource):
	class Meta:
		model = Comite


class ComiteAdmin(ImportExportModelAdmin, admin.ModelAdmin):
	list_display   = ('nombre', 'quien_administra', 'tipo_comite')
	search_fields = ['nombre']
	resource_class = ComiteResource


admin.site.register(Municipio, MunicipioAdmin)
admin.site.register(Administrador, AdministradorAdmin)
admin.site.register(Comite, ComiteAdmin)

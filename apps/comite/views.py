#import json
import os
import hashlib
import datetime
from io import BytesIO

from decimal import Decimal
#from django.core import serializers
from django.shortcuts import render, redirect
from django.views.generic import View
#from django.views.generic.list import ListView
from django.http import JsonResponse, HttpResponse

from reportlab.pdfgen import canvas
from reportlab.lib.colors import PCMYKColor
from reportlab.lib.pagesizes import letter, legal, landscape

#from apps.comite.models import Municipio
from apps.financiero.models import Corte
from .models import Municipio, Comite
from .forms import ComiteForm
from apps.bombero.models import Bombero


def error_404(request, exception):
    data = {}
    return render(request, '404.html', data)


def listComiteGeneral_view(request):
    return render(request, 'comite/listComiteGeneral.html', {})


def obteComiteGeneral_view(request):
    id_municipios      = [2, 3, 5, 6, 7, 8, 9, 10, 13, 14, 15, 16, 17, 18,]
    nombres_municipios = ['Cárdenas', 'Centla', 'Comalcalco', 'Cunduacán', 'Emiliano Zapata', 'Huimanguillo', 'Jalapa', 'Jalpa de Méndez', 'Nacajuca', 'Paraíso', 'Tacotalpa', 'Teapa', 'Tenosique', 'Zona Costera']
    if request.method == 'GET':
        estructura_final = []
        data = dict()
        for identificador in range(len(id_municipios)):
            datoComite = Comite.objects.filter(quien_administra='C.E.A.S.', municipio_id=id_municipios[identificador], registro_activo=True).count()
            estructura_final.append([id_municipios[identificador], nombres_municipios[identificador], datoComite])
        data = {'estructura_final': estructura_final}
        return JsonResponse(data)
    else:
        data = {'estructura_final': 'vacio'}
        return JsonResponse(data)


def listComiteMunicipio_view(request, municipio_id):
    listComite        = list(Comite.objects.filter(municipio_id=municipio_id, registro_activo=True).order_by('id').values('id', 'clasificacion_politica', 'nombre', 'nombre_inegi', 'quien_administra', 'latitud', 'longitud', 'tipo_comite', 'tiempo_funcion_comite_mes', 'tiempo_funcion_comite_anios', 'promotor', 'observaciones', 'municipio_id', 'municipio_id__nombre', 'presidente', 'secretario', 'tesorero', 'primer_vocal', 'segundo_vocal', 'tercer_vocal', 'bombero_id', 'bombero_id__nombre_completo', 'bombero_id__importe_adeudo', 'delegado_municipal', 'comisariado_ejidal', 'jefe_sector'))
    print(listComite)
#    id_comite         = listComite[0]['id']
    id_municipio      = listComite[0]['municipio_id']
    nombre_municipio  = listComite[0]['municipio_id__nombre']
#    bombero           = listComite[0]['bombero_id']
    #listFinanciero    = list(Corte.objects.filter(comite_id=id_comite, registro_activo=True).order_by('-fecha_corte').values('id', 'comite_id', 'fecha_corte', 'periodo_evaluado_inicio', 'periodo_evaluado_fin', 'folios_utilizados', 'saldo_caja', 'ingresos_porContratos', 'ingresos_porCuotasMensuales', 'ingresos_porAportaciones', 'ingresos_porOtros', 'total_ingresos', 'saldo_caja_mas_ingresos', 'total_egresos', 'saldo_final_caja', 'observaciones', 'alerta_observaciones', 'capturo'))
    cantidad_bomberos = Comite.objects.filter(quien_administra='C.E.A.S.', municipio_id=municipio_id, registro_activo=True).count()
    total_adeudo      = Decimal(0.00)
    for item in range(cantidad_bomberos):
        if listComite[item]['bombero_id__importe_adeudo'] != None:
#            importe      = Decimal(listComite[item]['bombero_id__importe_adeudo'])
            total_adeudo = total_adeudo + listComite[item]['bombero_id__importe_adeudo']
    if listComite:
        return render(request, 'comite/listComiteMunicipio.html', {'listComites': listComite, 'id_municipio': id_municipio, 'nombre_municipio': nombre_municipio, 'total_adeudo': total_adeudo})
    else:
        return render(request, 'comite/listComiteMunicipio.html', {'listComites': 'vacio'})


def buscMunicipio_view(request):
    # BUSQUEDA DE DATOS DE LOS MUNICIPIOS PARA EL MAPA DE LA PÁGINA PRINCIPAL
    if request.method == 'GET':
        dato = request.GET['opcion']
        if dato == 'buscar':
            dato_municipios = list(Municipio.objects.all().values('municipio_id', 'nombre', 'latitud_municipio', 'longitud_municipio', 'administrador__titulo', 'administrador__nombre', 'administrador__apellido_paterno', 'administrador__apellido_materno'))
            data            = dict()
            udesas          = []
            i               = 1
            while i <= (len(dato_municipios) - 1):
                dato_udesas = Comite.objects.filter(municipio=dato_municipios[i]['municipio_id'], tipo_comite='Udesa').count()
                dato_subudesas = Comite.objects.filter(municipio=dato_municipios[i]['municipio_id'], tipo_comite='Sub Udesa').count()
                udesas.append(dato_municipios[i]['municipio_id'])
                udesas.append(dato_municipios[i]['nombre'])
                udesas.append(dato_municipios[i]['latitud_municipio'])
                udesas.append(dato_municipios[i]['longitud_municipio'])
                udesas.append(str(dato_udesas))
                udesas.append(str(dato_subudesas))
                i += 1
            data = {'municipios': dato_municipios, 'udesas': udesas}
        return JsonResponse(data)


def formComiteGeneral_view(request):
    if request.method == 'POST':
        form = ComiteForm(request.POST)
        if form.is_valid():
            form.save()
            return redirect('listComiteGeneral')
    else:
        form = ComiteForm()
    return render(request, 'comite/formComite.html', {'form': form})


def formComite_view(request):
    if request.method == 'POST':
        form = ComiteForm(request.POST)
        if form.is_valid():
            form.save()
            return redirect('listComiteMunicipio', municipio_id=municipio_id)
    else:
        form = ComiteForm()
    return render(request, 'comite/formComite.html', {'form': form})


def updaComite_view(request, id):
    comite            = id
    comite_actualizar = Comite.objects.get(id=comite)
    municipio         = comite_actualizar.municipio_id
    if request.method == 'POST':
        form = ComiteForm(request.POST, instance=comite_actualizar)
        if form.is_valid():
            form.save()
            return redirect('listComiteMunicipio', municipio_id=municipio)
    else:
        form = ComiteForm(instance=comite_actualizar)
    return render(request, 'comite/updaComite.html', {'form': form})


def obtenDatosMapa_view(request):
    pass


def geneMapa_view(request):
    if request.method == 'GET':
        comite_id = request.GET['id']
        if os.path.isfile('/home/oscar/Proyectos/scids/static/img/ubicacion_comite/mapa' + str(comite_id) + '.png'):
            os.remove('/home/oscar/Proyectos/scids/static/img/ubicacion_comite/mapa' + str(comite_id) + '.png')
            mapa_ubicacion()
            respuesta = {'mapa': 'generado'}
            return render(request, 'comite/regiComite.html', {'respuesta': respuesta})
        else:
            mapa_ubicacion()
            respuesta = {'mapa': 'generado'}
            return render(request, 'comite/regiComite.html', {'respuesta': respuesta})
    else:
        respuesta = {'mapa': 'no generado'}
        return render(request, 'comite/regiComite.html', {'respuesta': respuesta})


def regiComite_view(request, id):
    comite            = id
    registro_comite   = Comite.objects.filter(id=comite)
    id_abastecido_por = list(Comite.objects.filter(id=comite).values('abastecido_por'))
    abastecedor       = id_abastecido_por[0]['abastecido_por']
    lista_cortes      = list(Corte.objects.filter(comite_id=comite).values())
    if lista_cortes !=0:
        cortes = lista_cortes
    else:
        cortes = ''

    if abastecedor != 0:
        nombre_abastecido_por = list(Comite.objects.filter(id=abastecedor).values('clasificacion_politica', 'nombre', 'nombre_inegi'))
    else:
        nombre_abastecido_por = ''
    return render(request, 'comite/regiComite.html', {'registro_comite': registro_comite, 'nombre_abastecido_por': nombre_abastecido_por, 'cortes': cortes})


class BorrComite_view(View):
    # BORRAR REGISTRO CON AYUDA DE AJAX
    def get(self, request):
        if request.method == 'GET':
            id                = request.GET['id']
            comite_eliminar   = Comite.objects.filter(id=id).update(registro_activo=False)
            data              = dict()
            data['respuesta'] = {'status': 'borrado'}
        return JsonResponse(data)

"""
 Checar los datos devueltos por la consulta,
 genera errores al recibir el segundo dato en cero o vacío.
"""
def buscComiteUpdate_view(request):
    if request.method == 'GET':
        municipio = request.GET['municipio']
        if municipio != 0:
            abastecido_por = request.GET.get('abastecido_por')
            if abastecido_por != '' or None:
                # Se obtiene una lista con los comités que correspondan a un municipio en especifico que viene en municipio
                dato_comite = list(Comite.objects.filter(municipio_id=municipio).values('id', 'clasificacion_politica', 'nombre'))
                comite_abastecedor = list(Comite.objects.filter(id=abastecido_por).values('id', 'clasificacion_politica', 'nombre'))
                if dato_comite:
                    data = dict()
                    data = {'encontrado': 'si', 'respuesta': dato_comite, 'comite_abastecedor': comite_abastecedor}
                    return JsonResponse(data)
                else:
                    data = {'encontrado': 'no'}
                    return JsonResponse(data)
            else:
                dato_comite = list(Comite.objects.filter(municipio_id=municipio).values('id', 'clasificacion_politica', 'nombre'))
                data = dict()
                data = {'encontrado': 'si', 'respuesta': dato_comite, 'comite_abastecedor': ''}
                return JsonResponse(data)


def encabezado_pie(c, usuario, pagina_no):
    # Colores
    white = PCMYKColor(0, 0, 0, 0)
    blue  = PCMYKColor(82, 45, 0, 4)
    #gray = PCMYKColor(0, 0, 0, 16)
    black = PCMYKColor(50, 0, 0, 100)
    # Firma
    hoy            = datetime.datetime.today()
    fecha          = hoy.strftime("%Y-%m-%d %H:%M:%S")
    dato_encriptar = usuario + fecha
    firma          = hashlib.md5(dato_encriptar.encode('utf-8')).hexdigest()

    # Encabezado
    c.setLineWidth(0.3)
    c.setFont('Helvetica', 13)
    c.setFillColor(black)
    c.drawImage('static/img/logo-dual.png', 770, 540, width=211, height=54)
    c.drawString(50, 560, 'COMISIÓN ESTATAL DE AGUA Y SANEAMIENTO')
    c.drawString(50, 546, 'DIRECTORIO DE COMITÉS UDESA')
    # Número de Página
    c.setFont('Helvetica', 9)
    c.drawString(720, 546, 'Pág. ' + str(pagina_no + 1))
    c.line(355, 546, 355, 572)
    c.setFont('Helvetica-Bold', 18)
    #c.drawString(360, 552, nombre_municipio.upper())
    c.line(50, 540, 760, 540)

    # Pie de Página
    c.setFont('Helvetica', 8)
    c.line(50, 50, 980, 50)
    c.setFillColor(black)
    c.drawString(818, 40, 'Coordinación Estatal de Agua y Saneamiento')
    c.drawString(871, 30, 'Dirección de Desarrollo Social')
    c.drawString(846, 20, 'Evaluación Administrativa a UDESAS')
    c.setFillColor(black, alpha=0.4)
    c.drawString(50, 40, 'Firma digital: ' + firma)
    c.drawString(50, 30, 'Reporte generado por: ' + usuario)
    c.drawString(50, 20, fecha)


def encabezado_tabla(c, blue, white):
    c.setFillColor(blue)
    c.rect(50, 500, 930, 30, fill=True, stroke=False)
    c.setFillColor(white)
    c.setFont('Helvetica-Bold', 9)
    c.drawString(54, 510, 'No.')
    c.drawString(80, 510, 'NOMBRE')
    c.drawString(320, 510, 'ADMINISTRA')
    c.drawString(400, 510, 'TIPO DE COMITÉ')
    c.drawString(500, 510, 'PROMOTOR')
    c.drawString(600, 510, 'BOMBERO / ADEUDO')
    #c.drawString(640, 510, 'SDO. CAJA + ING.')
    #c.drawString(740, 510, 'TOTAL DE EGRESOS')
    c.drawString(740, 510, 'OBSERVACIONES')


def repoGeneralComite_view(request):
    # Listado General de Comités por municipio
    response                        = HttpResponse(content_type='application/pdf')
    response['Content-Disposition'] = 'attachment; filename=reportGral_COMITÉS.pdf'
    buffer                          = BytesIO()
    c                               = canvas.Canvas(buffer, pagesize=landscape(legal))
    c.setTitle('reportGral_COMITÉS.pdf')
    w, h                            = letter
    usuario                         = request.user.username

    # Firma
    usuario                         = request.user.username
    hoy                             = datetime.datetime.today()
    fecha                           = hoy.strftime("%d-%m-%Y %H:%M:%S")
    dato_encriptar                  = fecha
    firma                           = hashlib.md5(dato_encriptar.encode('utf-8')).hexdigest()
    
    # Inseción del encabezado y pie de página
    #encabezado_pie(c, usuario)

    # Colores
    white                           = PCMYKColor(0, 0, 0, 0)
    black                           = PCMYKColor(50, 0, 0, 100)
    blue                            = PCMYKColor(74, 38, 0, 35)
    blue2                           = PCMYKColor(61, 12, 0, 14)
    green                           = PCMYKColor(84, 0, 100, 0)
    red                             = PCMYKColor(0, 100, 90, 0)

    # Contenido de la Página 1: Registros con Totales de Ingresos y Egresos
    # Encabezado de la tabla de datos
    #encabezado_tabla(c, blue, white)

    datos_comite       = list(Comite.objects.filter(registro_activo=True).order_by('municipio').values('clasificacion_politica', 'nombre', 'municipio', 'quien_administra', 'tipo_comite', 'promotor', 'bombero', 'bombero_id__nombre_completo', 'bombero_id__importe_adeudo', 'observaciones'))
    cantidad_registros = Comite.objects.filter(registro_activo=True).count()

    # separar la lista por municipios
    municipios = {
        2: "Cárdenas",
        3: "Centla",
        5: "Comalcalco",
        6: "Cunduacán",
        7: "Emiliano Zapata",
        8: "Huimanguillo",
        9: "Jalapa",
        10: "Jalpa de Méndez",
        13: "Nacajuca",
        14: "Paraíso",
        15: "Tacotalpa",
        16: "Teapa",
        17: "Tenosique",
        18: "Zona Costera"
    }

    cardenas = []

    for dato in datos_comite:
        print(municipios[dato['municipio']])

    cantidad_paginas   = cantidad_registros / 21
    dato = 1

    primer_linea       = 478
    siguiente_linea    = 20
    ultima_linea       = 21

    #for dato in range(len(datos_comite)):
    for pagina in range(int(cantidad_paginas)):
        pagina_no = int(pagina)
        # Datos
        
        c.showPage()

    c.save()
    pdf = buffer.getvalue()
    buffer.close()
    response.write(pdf)
    return response

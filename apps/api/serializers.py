from rest_framework import serializers
from apps.comite.models import Comite


class ComiteSerializers(serializers.ModelSerializer):
	class Meta:
		model  = Comite
		fields = ('id', 'clasificacion_politica', 'nombre_inegi', 'clave_inegi')

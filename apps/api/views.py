from django.shortcuts import render
from rest_framework import generics
from rest_framework.permissions import IsAuthenticated
from rest_framework.authentication import TokenAuthentication
from apps.comite.models import Comite
from .serializers import ComiteSerializers


class ComiteList(generics.ListCreateAPIView):
	queryset             = Comite.objects.all()
	serializer_class     = ComiteSerializers
	permission_classes   = (IsAuthenticated, )
	authentication_class = (TokenAuthentication, )

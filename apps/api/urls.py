from django.contrib import admin
from django.urls import path
from .views import ComiteList


urlpatterns = [
    path('listComite', ComiteList.as_view(), name='listComite'),
]

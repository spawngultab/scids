import os
import hashlib
import datetime
from io import BytesIO
from django.shortcuts import render
from django.http import HttpResponse
from reportlab.pdfgen import canvas
from reportlab.lib.colors import PCMYKColor
from reportlab.lib.pagesizes import letter, legal, landscape

from apps.comite.models import Municipio, Comite
from apps.financiero.models import Corte


def encabezadoPie(c, w, firma, usuario, fecha, blue, white, black, datos_comite, datos_comite_clasificacion_politica):
	#Encabezado
	c.setLineWidth(0.3)
	c.drawImage('static/img/logo-dual.png', 750, w - 80, width=211, height=54)
	c.setFillColor(blue)
	c.rect(50,520,680,60, fill=True, stroke=False)
	c.rect(50,514,912,2, fill=True, stroke=False)
	c.setFillColor(white)
	c.setFont('Helvetica-Bold', 14)
	c.drawString(60, 562, datos_comite[0]['municipio_id__nombre'])
	c.setFont('Helvetica', 12)
	c.drawString(240, 562, datos_comite_clasificacion_politica + ' ' + datos_comite[0]['nombre_inegi'])
	c.drawString(60, 546, 'Comité y Situación Financiera')
	c.drawString(240, 546, 'ADMINISTRA: ' + str(datos_comite[0]['quien_administra']))
	c.drawString(410, 546, 'T. COMITÉ: ' + str(datos_comite[0]['tipo_comite']))
	c.drawString(60, 530, 'T. DOMICILIARIAS: ' + str(datos_comite[0]['numero_tomas_domiciliarias']))
	c.drawString(240, 530, 'CUOTA: $' + str(datos_comite[0]['cuota_mensual_toma_domiciliaria']))

	# Pie de página
	c.setFont('Helvetica', 8)
	c.setFillColor(black)
	c.line(50, 68, 962, 68)
	c.drawString(814, 50, 'Comisión Estatal de Agua y Saneamiento')
	c.drawString(854, 40, 'Dirección de Desarrollo Social')
	c.drawString(828, 30, 'Evaluación Administrativa a UDESAS')

	c.setFillColor(black, alpha=0.3)
	c.drawString(50, 50, 'Firma digital: ' + firma)
	c.drawString(50, 40, 'Reporte generado por: ' + usuario)
	c.drawString(50, 30, fecha)


def imprimirReporte_view(request, id):
	# BOTON DE IMPRIMIR EN LA SECCIÓN DE DETALLE DE COMITÉ
	response                        = HttpResponse(content_type='application/pdf')
	response['Content-Disposition'] = 'attachment; filename=reportFinancieroComite.pdf'
	buffer                          = BytesIO()
	c                               = canvas.Canvas(buffer, pagesize=landscape(legal))
	w, h                            = legal
	id_comite                       = id
	usuario                         = request.user.username
	datos_comite                    = list(Comite.objects.filter(id=id_comite).values('clasificacion_politica', 'nombre_inegi', 'quien_administra', 'latitud', 'longitud', 'tipo_comite', 'abastecido_por', 'numero_tomas_domiciliarias', 'cuota_mensual_toma_domiciliaria', 'municipio_id__nombre'))
	datos_corte                     = list(Corte.objects.filter(comite_id=id).order_by('-fecha_corte').values('id', 'fecha_corte', 'periodo_evaluado_inicio', 'periodo_evaluado_fin', 'folios_utilizados', 'saldo_caja', 'ingresos_porContratos', 'ingresos_porCuotasMensuales', 'ingresos_porAportaciones', 'ingresos_porOtros', 'total_ingresos', 'saldo_caja_mas_ingresos', 'egresos_compensacionBombero', 'egresos_refaccionesMateriales', 'egresos_manoObra', 'egresos_energiaElectrica', 'egresos_pasajesViaticos', 'egresos_papeleria', 'egresos_telefono', 'egresos_gratificaciones', 'egresos_compraEquipo', 'egresos_otros', 'egresos_contratos', 'egresos_combustibles', 'total_egresos', 'saldo_final_caja'))

	# Firma
	hoy                             = datetime.datetime.today()
	fecha                           = hoy.strftime("%Y-%m-%d %H:%M:%S")
	dato_encriptar                  = usuario + fecha + str(id_comite)
	firma                           = hashlib.md5(dato_encriptar.encode('utf-8')).hexdigest()
	
	# Colores
	white                           = PCMYKColor(0, 0, 0, 0)
	black                           = PCMYKColor(50, 0, 0, 100)
	blue                            = PCMYKColor(74, 38, 0, 35)
	blue2                           = PCMYKColor(61, 12, 0, 14)
	green                           = PCMYKColor(84, 0, 100, 0)
	red                             = PCMYKColor(0, 100, 90, 0)

	if datos_comite[0]['clasificacion_politica'] == None:
		datos_comite_clasificacion_politica = 'Indef.'
	else:
		datos_comite_clasificacion_politica = datos_comite[0]['clasificacion_politica']

	# Encabezado
	encabezadoPie(c, w, firma, usuario, fecha, blue, white, black, datos_comite, datos_comite_clasificacion_politica)

	# Contenido de la Página 1: Registros con Totales de Ingresos y Egresos
	if datos_corte:
		c.setFillColor(blue2)
		c.rect(50,490,912,20, fill=True, stroke=False)
		c.setFillColor(white)
		c.setFont('Helvetica-Bold', 10)
		c.drawString(54, 496, 'No.')
		c.drawString(80, 496, 'FECHA DE CORTE')
		c.drawString(190, 496, 'PERIODO EVALUADO')
		c.drawString(320, 496, 'FOLIOS UTILIZADOS')
		c.drawString(440, 496, 'SDO. CAJA')
		c.drawString(520, 496, 'TOTAL DE INGRESOS')
		c.drawString(640, 496, 'SDO. CAJA + ING.')
		c.drawString(740, 496, 'TOTAL DE EGRESOS')
		c.drawString(880, 496, 'SDO. F. CAJA')

		registros = len(datos_corte)
		fila      = 478

		if datos_corte[0]['saldo_final_caja'] < 0:
			c.setFillColor(red)
		else:
			c.setFillColor(green)

		c.drawString(54, 478, str('1'))
		c.drawString(80, 478, str(datos_corte[0]['fecha_corte']))
		c.drawString(190, 478, str(datos_corte[0]['periodo_evaluado_inicio']) + ', ' + str(datos_corte[0]['periodo_evaluado_fin']))
		c.drawString(320, 478, str(datos_corte[0]['folios_utilizados']))
		c.drawString(440, 478, str(datos_corte[0]['saldo_caja']))
		c.drawString(520, 478, str(datos_corte[0]['total_ingresos']))
		c.drawString(640, 478, str(datos_corte[0]['saldo_caja_mas_ingresos']))
		c.drawString(740, 478, str(datos_corte[0]['total_egresos']))
		c.drawString(880, 478, str(datos_corte[0]['saldo_final_caja']))

		c.setFillColor(black)
		c.setFont('Helvetica', 10)

		# ESTE CICLO SE USA PARA LA IMPRESIÓN DE LA LISTA DE FOLIOS USADOS
		i           = 0
		h           = 0
		fila_actual = 464 # A PARTIR DEL SEGUNDO REGISTRO SE INICIA LA IMPRESIÓN YA QUE EL PRIMERO SE IMPRIME FUERA DEL CICLO

		while i < registros:
			folios           = str(datos_corte[i]['folios_utilizados'])
			folios_separados = folios.split(', ')
			cantidad_folios  = len(folios_separados)
			lista_folios     = folios_separados[i]
			siguiente        = i * 14

			if cantidad_folios != 1:
				c.drawString(54, fila - siguiente, str(i + 1))
				c.drawString(80, fila - siguiente, str(datos_corte[i]['fecha_corte']))
				c.drawString(190, fila - siguiente, str(datos_corte[i]['periodo_evaluado_inicio']) + ', ' + str(datos_corte[i]['periodo_evaluado_fin']))
				c.drawString(440, fila - siguiente, str(datos_corte[i]['saldo_caja']))
				c.drawString(520, fila - siguiente, str(datos_corte[i]['total_ingresos']))
				c.drawString(640, fila - siguiente, str(datos_corte[i]['saldo_caja_mas_ingresos']))
				c.drawString(740, fila - siguiente, str(datos_corte[i]['total_egresos']))
				c.drawString(880, fila - siguiente, str(datos_corte[i]['saldo_final_caja']))

				while h < cantidad_folios:
					siguiente_fila = 14
					c.drawString(320, fila_actual - (siguiente_fila * h), str(folios_separados[h]))
					h = h + 1
			i = i + 1

		c.showPage()
		# Encabezado
		encabezadoPie(c, w, firma, usuario, fecha, blue, white, black, datos_comite, datos_comite_clasificacion_politica)

		# Contenido de la Página 2: Desgloce de Ingresos y Egresos
		c.setFillColor(blue2)
		c.rect(50,490,240,20, fill=True, stroke=False)
		c.setFillColor(red)
		c.rect(290,490,580,20, fill=True, stroke=False)
		c.setFillColor(white)
		c.setFont('Helvetica-Bold', 10)
		c.drawString(60, 496, 'No.')
		c.drawString(90, 496, 'INGRESOS')
		c.drawString(300, 496, 'No.')
		c.drawString(330, 496, 'EGRESOS')

		c.setFont('Helvetica', 12)
		c.setFillColor(black)

		# Datos de la segunda hoja (detalles)
		contador = 1
		for i in datos_corte:
			c.drawString(60, 478, str(contador))
			if i['ingresos_porContratos'] != None:
				c.drawString(90, 478, 'por Contratos: $' + str(i['ingresos_porContratos']))
			else:
				c.drawString(90, 478, 'por Contratos: ')
			if i['ingresos_porCuotasMensuales'] != None:
				c.drawString(90, 464, 'por Cuotas Mensuales: $' + str(i['ingresos_porCuotasMensuales']))
			else:
				c.drawString(90, 464, 'por Cuotas Mensuales: ')
			if i['ingresos_porAportaciones'] != None:
				c.drawString(90, 450, 'por Aportaciones: $' + str(i['ingresos_porAportaciones']))
			else:
				c.drawString(90, 450, 'por Aportaciones: ')
			if i['ingresos_porOtros'] != None:
				c.drawString(90, 436, 'por Otros: $' + str(i['ingresos_porOtros']))
			else:
				c.drawString(90, 436, 'por Otros: ')
			if i['egresos_compensacionBombero'] != None:
				c.drawString(330, 478, 'por Compensación al Bombero: $' + str(i['egresos_compensacionBombero']))
			else:
				c.drawString(330, 478, 'por Compensación al Bombero: ')
			if i['egresos_refaccionesMateriales'] != None:
				c.drawString(330, 464, 'por Compra de Refacciones y Materiales: $' + str(i['egresos_refaccionesMateriales']))
			else:
				c.drawString(330, 464, 'por Compra de Refacciones y Materiales: ')
			if i['egresos_manoObra'] != None:
				c.drawString(330, 450, 'por Mano de Obra: $' + str(i['egresos_manoObra']))
			else:
				c.drawString(330, 450, 'por Mano de Obra: ')
			if i['egresos_energiaElectrica'] != None:
				c.drawString(330, 436, 'por Pago de Energía Eléctrica: $' + str(i['egresos_energiaElectrica']))
			else:
				c.drawString(330, 436, 'por Pago de Energía Eléctrica: ')
			if i['egresos_pasajesViaticos'] != None:
				c.drawString(330, 422, 'por Pasajes y Viáticos: $' + str(i['egresos_pasajesViaticos']))
			else:
				c.drawString(330, 422, 'por Pasajes y Viáticos: ')
			if i['egresos_papeleria'] != None:
				c.drawString(330, 408, 'por Compra de Papelería: $' + str(i['egresos_papeleria']))
			else:
				c.drawString(330, 408, 'por Compra de Papelería: ')
			if i['egresos_telefono'] != None:
				c.drawString(630, 478, 'por Compra de Saldo Telefónico: $' + str(i['egresos_telefono']))
			else:
				c.drawString(630, 478, 'por Compra de Saldo Telefónico: ')
			if i['egresos_gratificaciones'] != None:
				c.drawString(630, 464, 'por Pago de Gratificaciones: $' + str(i['egresos_gratificaciones']))
			else:
				c.drawString(630, 464, 'por Pago de Gratificaciones: ')
			if i['egresos_compraEquipo'] != None:
				c.drawString(630, 450, 'por Compra de Equipo: $' + str(i['egresos_compraEquipo']))
			else:
				c.drawString(630, 450, 'por Compra de Equipo: ')
			if i['egresos_otros'] != None:
				c.drawString(630, 436, 'por Otros: $' + str(i['egresos_otros']))
			else:
				c.drawString(630, 436, 'por Otros: ')
			if i['egresos_contratos'] != None:
				c.drawString(630, 422, 'por Pago de Contratos: $' + str(i['egresos_contratos']))
			else:
				c.drawString(630, 422, 'por Pago de Contratos: ')
			if i['egresos_combustibles'] != None:
				c.drawString(630, 408, 'por Compra de Combustibles: $' + str(i['egresos_combustibles']))
			else:
				c.drawString(630, 408, 'por Compra de Combustibles: ')
			contador += 1
	else:
		c.setFillColor(black)
		c.setFont('Helvetica', 14)
		c.drawString(420, 408, 'Sin registros financieros')

	# Mapa
	#c.drawImage('static/img/mapa.png', 350, h - 112, width=211, height=54)

	c.showPage()
	c.save()
	pdf = buffer.getvalue()
	buffer.close()
	response.write(pdf)
	return response

def repoComite_view(request, id):
	pass
"""
	response                        = HttpResponse(content_type='application/pdf')
	response['Content-Disposition'] = 'attachment; filename=reportComite_UDESA.pdf'
	buffer                          = BytesIO()
	c                               = canvas.Canvas(buffer, pagesize=letter)
	w, h                            = letter
	id_comite                       = id
	usuario                         = request.user.username
	datos_comite                    = list(Comite.objects.filter(id=id_comite).values('clasificacion_politica', 'nombre', 'quien_administra', 'latitud', 'longitud'))
	
	# Firma
	hoy                             = datetime.datetime.today()
	fecha                           = hoy.strftime("%Y-%m-%d %H:%M:%S")
	dato_encriptar                  = usuario + fecha + str(id_comite)
	firma                           = hashlib.md5(dato_encriptar.encode('utf-8')).hexdigest()
	
	# Colores
	white                           = PCMYKColor(0, 0, 0, 0)
	black                           = PCMYKColor(50, 0, 0, 100)
	blue                            = PCMYKColor(74, 38, 0, 35)
	blue2                           = PCMYKColor(61, 12, 0, 14)

	# Encabezado
	c.setLineWidth(0.3)
	c.setFont('Helvetica', 13)

	c.drawImage('static/img/logo-dual.png', 350, h - 112, width=211, height=54)
	c.drawString(50, h - 50, 'DETALLE DE COMITÉ')
	c.drawString(440, h - 50, fecha)
	x = 50
	y = h - 54
	c.line(x, y, x + 512, y)

	c.setFillColor(blue)
	c.rect(50, h - 110, 290, 50, fill=True, stroke=False)
	c.setFillColor(white)
	c.setFont('Helvetica', 11)
	c.drawString(60, h - 80, str(datos_comite[0]['clasificacion_politica']) + ' ' + str(datos_comite[0]['nombre']))
	c.drawString(60, h - 96, 'Ubicación (Latitud, Longitud): ')

	c.setFillColor(blue2)
	c.rect(50, h - 150, 512, 30, fill=True, stroke=False)
	c.setFillColor(black)
	c.setFont('Helvetica-Bold', 9)
	c.drawString(60, h - 140, 'Comunidad')
	c.drawString(220, h - 140, 'Tipo de Comité')
	c.drawString(300, h - 140, 'No. Habitantes')
	c.drawString(370, h - 140, 'Cant. Tomas Dom.')
	c.drawString(470, h - 140, 'Cuota Tomas Dom.')

	# Mapa
	c.drawImage('static/img/mapa.png', 350, h - 112, width=211, height=54)

	# Pie de Página
	c.setFont('Helvetica', 8)

	x = 50
	y = h - 732
	c.line(x, y, x + 512, y)
	c.drawString(400, 50, 'Coordinación Estatal de Agua y Saneamiento')
	c.drawString(454, 40, 'Dirección de Desarrollo Social')
	c.drawString(428, 30, 'Evaluación Administrativa a UDESAS')

	c.setFillColor(black, alpha=0.3)
	c.drawString(50, 50, 'Firma digital: ' + firma)
	c.drawString(50, 40, 'Reporte generado por: ' + usuario)

	c.showPage()
	c.save()
	pdf = buffer.getvalue()
	buffer.close()
	response.write(pdf)
	return response
"""
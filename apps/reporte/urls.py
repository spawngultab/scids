from django.contrib.auth.decorators import login_required
from django.urls import path
from apps.reporte import views


urlpatterns = [
    path('repoComite/<int:id>/', views.repoComite_view, name='repoComite'),
    path('imprimir/<int:id>/', login_required(views.imprimirReporte_view), name='imprimir'),
]
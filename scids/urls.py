"""scids URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/3.0/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.contrib import admin
from django.urls import path, include
from django.views.generic.base import TemplateView
from django.contrib.auth.decorators import login_required
from django.conf import settings
from django.conf.urls import handler404
from rest_framework.authtoken import views
from apps.comite.views import error_404

#handler404 = error_404
handler404 = 'apps.comite.views.error_404'

urlpatterns = [
    path('admin/', admin.site.urls),
    path('api/v1.0/', include('apps.api.urls')),
    path('obtener_tokenAPI/', views.obtain_auth_token),
    path('', login_required(TemplateView.as_view(template_name='inicio.html')), name='inicio'),
    path('accounts/', include('django.contrib.auth.urls')),
    path('bombero/', include('apps.bombero.urls')),
    path('cfe/', include('apps.cfe.urls')),
    path('comite/', include('apps.comite.urls')),
    path('financiero/', include('apps.financiero.urls')),
    path('reporte/', include('apps.reporte.urls')),
]

if settings.DEBUG:
    from django.conf.urls.static import static
    urlpatterns += static(settings.STATIC_URL, document_root=settings.STATIC_ROOT)
    #urlpatterns += static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)

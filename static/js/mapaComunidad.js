var latitud = $("#latComunidad").val();
var longitud = $("#lonComunidad").val();

if ((latitud != undefined) && (longitud != undefined)) {
    var mymap = L.map('mapaComite').setView([latitud, longitud], 11);

    L.tileLayer('https://api.tiles.mapbox.com/v4/{id}/{z}/{x}/{y}.png?access_token=pk.eyJ1IjoibWFwYm94IiwiYSI6ImNpejY4NXVycTA2emYycXBndHRqcmZ3N3gifQ.rJcFIG214AriISLbB6B5aw', {
        maxZoom: 18,
        attribution: 'Map data &copy; <a href="https://www.openstreetmap.org/">OpenStreetMap</a> contributors, ' +
            '<a href="https://creativecommons.org/licenses/by-sa/2.0/">CC-BY-SA</a>',
        id: 'mapbox.streets'
    }).addTo(mymap);

    var marker = L.marker([latitud, longitud]).addTo(mymap).on('click', function(e){
        var latitud = e.latlng.lat;
        var longitud = e.latlng.lng;
    });

    if (mymap.scrollWheelZoom.enabled()) {
        mymap.scrollWheelZoom.disable();
    }
} else {
    //$("#seccionMapa").hide('fast');
    $("#seccionMapa").html('<hr><h5>Mapa no disponible</h5>');
}

function generaMapa() {}
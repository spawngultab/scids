$(document).ready(function (){
    $("#cuadroBuscar").focus();
    $('[data-toggle="tooltip"]').tooltip();

    $("#cuadroBuscar").on('keyup', function(){
        var value = $(this).val().toLowerCase();
        $("#tablaBomberos tbody tr").filter(function(){
            $(this).toggle($(this).text().toLowerCase().indexOf(value) > -1)
        });
    });
});

function borrBombero(id) {
    $.ajax({
        data: {'id': id},
        url: 'borrBombero/',
        dataType: 'json',
        type: 'GET',
        success: function(data) {
            if (data.respuesta.status == 'borrado') {
                $("#avisos").html('<span class="badge badge-warning">Registro eliminado</span>');
            } else {
                console.log('No pudo borrarse el registro');
            }
        }
    });
}

var latitud = 17.9868908;
var longitud = -92.9302826;
var mymap = L.map('mapid').setView([latitud, longitud], 9);

var mapbox = L.tileLayer('https://api.tiles.mapbox.com/v4/{id}/{z}/{x}/{y}.png?access_token=pk.eyJ1IjoibWFwYm94IiwiYSI6ImNpejY4NXVycTA2emYycXBndHRqcmZ3N3gifQ.rJcFIG214AriISLbB6B5aw', {
    maxZoom: 18,
    attribution: 'Map data &copy; <a href="https://www.openstreetmap.org/">OpenStreetMap</a> contributors, ' +
        '<a href="https://creativecommons.org/licenses/by-sa/2.0/">CC-BY-SA</a>',
    id: 'mapbox.streets',
    updateWhenIdle: true,
    reuseTiles: true
}).addTo(mymap);
var osm = L.tileLayer('https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png?', {
    maxZoom: 18,
    attribution: 'Map data &copy; <a href="https://www.openstreetmap.org/">OpenStreetMap</a> contributors, <a href="https://creativecommons.org/licenses/by-sa/2.0/">CC-BY-SA</a>',
    id: 'osm.streets',
    updateWhenIdle: true,
    reuseTiles: true
}).addTo(mymap);

    L.control.layers({
      "OpenStreetMap": osm,
      "MapBox": mapbox
    }).addTo(mymap);

$.ajax({
    data: {'opcion': 'buscar'},
    url: '/comite/buscMunicipio/',
    dataType: 'json',
    type: 'GET',
    success: function(data) {
        for (i = 0; i < Object.keys(data.municipios).length; i++) {
            var municipio_id   = data.municipios[i]['municipio_id'];
            var nombre         = data.municipios[i]['nombre'];
            var latitud        = data.municipios[i]['latitud_municipio'];
            var longitud       = data.municipios[i]['longitud_municipio'];
            var admin_titulo   = data.municipios[i]['administrador__titulo'];
            var admin_nombre   = data.municipios[i]['administrador__nombre'];
            var admin_apaterno = data.municipios[i]['administrador__apellido_paterno'];
            var admin_amaterno = data.municipios[i]['administrador__apellido_materno'];
            //var udesas         = data.udesas;
            
            marcador_mapa(municipio_id, nombre, latitud, longitud, admin_titulo, admin_nombre, admin_apaterno, admin_amaterno);
        }
    }
});

function marcador_mapa(municipio_id, nombre, latitud, longitud, admin_titulo, admin_nombre, admin_apaterno, admin_amaterno) {
    var marker = L.marker([latitud, longitud]).addTo(mymap).on('click', function(e){
        var latitud = e.latlng.lat;
        var longitud = e.latlng.lng;
    });

    if (admin_nombre != null) {
        marker.bindPopup('<a href="#" onclick="panelMapa(' + municipio_id + ');"><b>' + nombre + '</a><hr>Administrador:</b><br> ' + admin_titulo + ' ' + admin_nombre + ' ' + admin_apaterno + ' ' + admin_amaterno + '<br><b>Ubicación:</b> ' + latitud + ', ' + longitud + '<br><br><img src="static/img/mapa/' + municipio_id + '.jpg">');
    } else {
        marker.bindPopup('<a href="#" onclick="panelMapa(' + municipio_id + ');"><b>' + nombre + '</a><br>Administrador:</b> No disponible<hr><b>Ubicación:</b> ' + latitud + ', ' + longitud + '<br><br><img src="static/img/mapa/' + municipio_id + '.jpg">');
    }
}

function panelMapa(municipio_id) {
    $(location).attr('href', 'comite/listComiteMunicipio/' + municipio_id);
}
